#!/usr/bin/env bash

if [ "$1" == "dev" ] || [ "$1" == "prod" ];
then
  ENV=$1;

  PATH_ENV=""
  oneSignalAppId=""
  webClientId=""
  if [ "$1" == "dev" ];
  then
    API_PATH="https://southamerica-east1-carsapp-dev.cloudfunctions.net/app/";
    oneSignalAppId="d4d1fffe-0c20-4960-b99b-7d8263a3a3cb";
    webClientId="165888005406-vqljm827difd52cn1b45a20rtdaglmu8";
  fi
  if [ "$1" == "prod" ];
  then
    API_PATH="https://southamerica-east1-carsapp-dev.cloudfunctions.net/app/";
    oneSignalAppId="";
  fi

  echo "Switching to Firebase to $ENV"
   cp -rf "firebaseEnviroments/android/$ENV/google-services.json" android/app/
   cp -rf "firebaseEnviroments/android/$ENV/autodato.keystore" android/app/
   cp -rf "firebaseEnviroments/ios/$ENV/GoogleService-Info.plist" ios/carsapp_mobile/
   # cp -rf "firebaseEnviroments/ios/$ENV/Info.plist" ios/carsapp_mobile/
   echo "export default { env: '$ENV', path: '$API_PATH', oneSignalAppId: '$oneSignalAppId', webClientId: '$webClientId' };" > env.js
  echo "Finish"
  else
    echo "Missing paramenter, please execute sh set-enviroment.sh dev"
fi
