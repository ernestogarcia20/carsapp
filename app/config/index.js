import ENV from '../../env';

export default {
  urlApi: ENV.path,
  oneSignalAppId: ENV.oneSignalAppId,
};
