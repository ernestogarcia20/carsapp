import React from 'react';
import {connect, Provider} from 'react-redux';
import 'react-native-gesture-handler';
import {PersistGate} from 'redux-persist/integration/react';
import {createReduxContainer} from 'react-navigation-redux-helpers';
import {persistor, store} from './stores/redux-config';
import AppNavigator from './navigation/AppNavigator';
import GlobalTask from './GlobalTask';

const ReduxContainerComponent = createReduxContainer(AppNavigator);

const mapStateToProps = (state) => ({
  state: state.navigation,
});

const AppWithNavigationState = connect(mapStateToProps)(ReduxContainerComponent);

const App = () => {
  return (
    <Provider store={store}>
      <PersistGate persistor={persistor} loading={null}>
        <GlobalTask>
          <AppWithNavigationState />
        </GlobalTask>
      </PersistGate>
    </Provider>
  );
};

export default App;
