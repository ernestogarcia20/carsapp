import {applyMiddleware, createStore} from 'redux';
import {persistReducer, persistStore} from 'redux-persist';
import {createReactNavigationReduxMiddleware} from 'react-navigation-redux-helpers';

import thunk from 'redux-thunk';
import AsyncStorage from '@react-native-community/async-storage';
import reducer from './reducers';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  blacklist: ['navigation'],
};

const middleware = createReactNavigationReduxMiddleware((state) => state.navigation, 'root');

const store = createStore(
  persistReducer(persistConfig, reducer),
  applyMiddleware(middleware, thunk)
);

const persistor = persistStore(store);

export {store, persistor};
