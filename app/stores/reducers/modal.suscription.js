import {SET_MODAL_SUSCRIPTION, SET_MODAL_SUSCRIPTION_STARTED} from '@constants/reducers';

/**
 * @return {boolean}
 */
function modalSuscriptionReducer(state = false, action) {
  switch (action.type) {
    case SET_MODAL_SUSCRIPTION:
      return action.payload;
    case SET_MODAL_SUSCRIPTION_STARTED:
      return false;
    default:
      return state;
  }
}

export default modalSuscriptionReducer;
