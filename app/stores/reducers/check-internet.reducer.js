import {SET_INTERNET, REMOVE_ALL} from '@constants/reducers';

/**
 * @return {boolean | Object}
 */
function checkInternet(state = true, action) {
  switch (action.type) {
    case SET_INTERNET:
      return action.payload;
    case REMOVE_ALL:
      return true;
    default:
      return state;
  }
}

export default checkInternet;
