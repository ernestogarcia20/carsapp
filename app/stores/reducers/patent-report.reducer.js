import {SET_PATENT_REPORT, REMOVE_ALL} from '@constants/reducers';

const INITIAL_STATE = {
  patentReport: [],
};
function patentReportReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case SET_PATENT_REPORT:
      return {...state, patentReport: action.payload};
    case REMOVE_ALL:
      return INITIAL_STATE;
    default:
      return {...state};
  }
}

export default patentReportReducer;
