import {SET_ONBOARD, REMOVE_ALL} from '@constants/reducers';

const INITIAL_STATE = {
  onboardSlides: [],
};
function onboardReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case SET_ONBOARD:
      return {...state, onboardSlides: action.payload};
    case REMOVE_ALL:
      return INITIAL_STATE;
    default:
      return {...state};
  }
}

export default onboardReducer;
