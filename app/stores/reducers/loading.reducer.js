import {REMOVE_ALL, SET_LOADING} from '@constants/reducers';

/**
 * @return {boolean | Object}
 */
function loadingReducer(state = false, action) {
  switch (action.type) {
    case SET_LOADING:
      return action.payload;
    case REMOVE_ALL:
      return false;
    default:
      return state;
  }
}

export default loadingReducer;
