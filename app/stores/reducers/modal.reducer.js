import {SET_MODAL, REMOVE_ALL} from '@constants/reducers';

const INITIAL_STATE = {
  isVisible: false,
};
function modalReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case SET_MODAL:
      return {...state, ...action.payload};
    case REMOVE_ALL:
      return INITIAL_STATE;
    default:
      return {...state};
  }
}

export default modalReducer;
