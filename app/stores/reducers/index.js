import {combineReducers} from 'redux';

import app from './app';
import navigation from './navigation';
import user from './user.reducer';
import onBoard from './onboard.reducer';
import loading from './loading.reducer';
import checkInternet from './check-internet.reducer';
import patentReport from './patent-report.reducer';
import modal from './modal.reducer';
import deviceToken from './device-token.reducer';
import dispatch from './dispatch';
import showModalSuscription from './modal.suscription';

export default combineReducers({
  app,
  navigation,
  user,
  onBoard,
  loading,
  checkInternet,
  dispatch,
  showModalSuscription,
  patentReport,
  modal,
  deviceToken,
});
