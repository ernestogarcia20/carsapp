/*
  User Reducer, is used to manage user data and persist them.
 */
import {SET_DEVICE_TOKEN} from '../../utils/constants/reducers';

/**
 * @return {null | string}
 */
function deviceToken(state = null, action) {
  switch (action.type) {
    case SET_DEVICE_TOKEN:
      return action.payload;
    default:
      return state;
  }
}

export default deviceToken;
