
import {SET_DISPATCH, REMOVE_ALL} from '../../utils/constants/reducers';

/**
 * @return {Function | Object}
 */
function dispatch(state = () => {}, action) {
  switch (action.type) {
    case SET_DISPATCH:
      return action.payload;
    case REMOVE_ALL:
      return () => {};
    default:
      return state;
  }
}

export default dispatch;
