import {SET_MODAL_SUSCRIPTION, SET_USER, UPDATE_USER} from '@constants/reducers';
import {setStore} from '@functions/globals';
import {AppApi} from '@services/index';
import {getUserAction} from '@actions/auth.actions';

export const suscriptionAction = (obj) => {
  return async (dispatch) => {
    const {status, data} = await AppApi.SetPremium(obj);
    if (status !== 200 || !data || typeof data !== 'object') {
      throw data;
    }
    if (data.status === 'expired') {
      throw data;
    }
    dispatch(setStore(UPDATE_USER, data?.user));
    return data;
  };
};

export const checkSuscriptionAction = (originalTransactionId) => {
  return async () => {
    const {status, data} = await AppApi.CheckSubscription(originalTransactionId);
    if (status !== 200) {
      throw data;
    }
    return data?.exists;
  };
};

export const moveSuscriptionAction = (obj, deviceToken) => {
  return async (dispatch) => {
    const {status, data} = await AppApi.MoveSubscription(obj);
    if (status !== 200) {
      throw data;
    }
    dispatch(getUserAction(deviceToken));
    return data;
  };
};

export const setModalSuscriptionAction = (showModal = false) => {
  return async (dispatch) => {
    dispatch(setStore(SET_MODAL_SUSCRIPTION, showModal));
  };
};

export const cancelSuscription = (sku) => {
  return async (dispatch) => {
    // dispatch(setStore(SET_INTERNET, hasInternet));
  };
};
