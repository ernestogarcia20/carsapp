import {SET_LOADING} from '@constants/reducers';
import {setStore} from '@functions/globals';

export const setLoadingAction = (loading = false) => {
  return async dispatch => {
    dispatch(setStore(SET_LOADING, loading));
  };
};
