import {SET_DISPATCH} from '@constants/reducers';
import {setStore} from '@functions/globals';

export const setDispatchAction = (dispatchFunction = () => {}) => {
  return async dispatch => {
    dispatch(setStore(SET_DISPATCH, dispatchFunction));
  };
};
