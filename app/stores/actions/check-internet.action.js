import {SET_INTERNET, REMOVE_ALL} from '@constants/reducers';
import {setStore} from '@functions/globals';

export const changeInternetAction = hasInternet => {
  return async dispatch => {
    dispatch(setStore(SET_INTERNET, hasInternet));
  };
};
