import {SET_MODAL} from '@constants/reducers';
import {setStore} from '@functions/globals';
import {ModalActionSheet} from '../../utils/interface/interfaceHelper';

export const setModalAction = (modal: ModalActionSheet) => {
  return async dispatch => {
    dispatch(setStore(SET_MODAL, modal));
  };
};
