import {SET_MODAL_SUSCRIPTION, SET_USER, UPDATE_USER} from '@constants/reducers';
import {setStore} from '@functions/globals';
import {AppApi} from '@services/index';

export const homeAction = (deviceToken) => {
  return async (dispatch) => {
    const {status, data} = await AppApi.GetHome(deviceToken);
    if (status !== 200) {
      throw data;
    }
    if (data?.user) {
      dispatch(
        setStore(SET_USER, {
          ...(data?.user || {}),
          reports_groups: data?.user?.reports_groups || [],
        })
      );
    }
    return data;
  };
};

export const setModalSuscriptionAction = (showModal = false) => {
  return async (dispatch) => {
    dispatch(setStore(SET_MODAL_SUSCRIPTION, showModal));
  };
};

export const cancelSuscription = (sku) => {
  return async (dispatch) => {
    // dispatch(setStore(SET_INTERNET, hasInternet));
  };
};
