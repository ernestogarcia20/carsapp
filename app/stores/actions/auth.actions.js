import {AppApi} from '@services/index';
import {
  SET_ONBOARD,
  SET_USER,
  REMOVE_ALL,
  UPDATE_USER,
  SET_DEVICE_TOKEN,
} from '@constants/reducers';
import {GoogleSignin} from '@react-native-community/google-signin';
import {LoginManager, AccessToken} from 'react-native-fbsdk';
import {appleAuth} from '@invertase/react-native-apple-authentication';
import auth from '@react-native-firebase/auth';
import {setStore} from '@functions/globals';
import {Alert} from 'react-native';
import {setLoadingAction} from './loading.actions';
import ENV from '../../../env';

GoogleSignin.configure({
  webClientId: ENV.webClientId,
});

export const logOutAction = () => {
  return async (dispatch) => {
    try {
      LoginManager.logOut();
      await GoogleSignin.signOut();
      await auth().signOut();
      dispatch(setStore(REMOVE_ALL));
    } catch (e) {
      dispatch(setStore(REMOVE_ALL));
      throw e;
    }
  };
};

export const setDeviceTokenStoreAction = (devicetoken) => {
  return async (dispatch) => {
    dispatch(setStore(SET_DEVICE_TOKEN, devicetoken));
  };
};

export const updateUserStoreAction = (data) => {
  return async (dispatch) => {
    dispatch(setStore(UPDATE_USER, data));
  };
};

export const getUserAction = (deviceToken = null) => {
  return async (dispatch) => {
    const {data, status} = await AppApi.GetUser(deviceToken);
    if (data.user_not_found) {
      return false;
    }
    if (status !== 200) {
      throw data;
    }
    try {
      dispatch(setStore(SET_USER, {...data?.user, reports_groups: data?.reports_groups || []}));
      return data;
    } catch (e) {
      dispatch(logOutAction());
      throw e;
    }
  };
};

export const createAccountAction = (user) => {
  return async (dispatch) => {
    const {data, status} = await AppApi.CreateUser(user);
    if (status !== 200) {
      throw data;
    }
    return dispatch(
      setStore(SET_USER, {...data?.user, reports_groups: data?.reports_groups || []})
    ); // dispatch(getUserAction(user?.device_token || null));
  };
};

export const getOnBoardAction = (finishLoading = true) => {
  return async (dispatch) => {
    dispatch(setLoadingAction(true));
    try {
      const {status, data} = await AppApi.GetOnBoard();
      if (status !== 200 && status !== 201) {
        dispatch(setLoadingAction(false));
        throw data;
      }
      const data2 = Object.keys(data).map((x) => {
        return {...data[x]};
      });
      dispatch(setStore(SET_ONBOARD, data2));
      if (finishLoading) {
        dispatch(setLoadingAction(false));
      }
      return {slidesFounded: data};
    } catch (e) {
      dispatch(setLoadingAction(false));
      throw e;
    }
  };
};

export const signInGoogleAction = (deviceToken) => {
  return async (dispatch) => {
    // Get the users ID token
    const {idToken} = await GoogleSignin.signIn();
    // Create a Google credential with the token
    const googleCredential = auth.GoogleAuthProvider.credential(idToken);

    // Sign-in the user with the credential
    const credential = auth().signInWithCredential(googleCredential);
    // const validateUser = await validateUserFirestore((await credential).user.uid);
    const user = {
      id: (await credential).user.uid,
      firstname: (await credential).user.displayName
        ? (await credential).user.displayName.split(' ')[0]
        : '',
      lastname: (await credential).user.displayName
        ? (await credential).user.displayName.split(' ')[1]
        : '',
      fullname: (await credential)?.user?.displayName || '',
      email: (await credential).user.email || '',
      gender: '',
      birthdate: '',
      device_token: deviceToken,
    };
    const responseUser = await dispatch(getUserAction(deviceToken));
    if (responseUser) {
      return responseUser;
    }
    return dispatch(createAccountAction(user));
  };
};

export const signInFacebookAction = (deviceToken) => {
  return async (dispatch) => {
    // Attempt login with permissions
    try {
      const result = await LoginManager.logInWithPermissions(['public_profile', 'email']);
      if (result.isCancelled) {
        throw result;
      }
      // Once signed in, get the users AccesToken
      const data = await AccessToken.getCurrentAccessToken();

      if (!data) {
        throw data;
      }

      // Create a Firebase credential with the AccessToken
      const facebookCredential = auth.FacebookAuthProvider.credential(data.accessToken);
      // Sign-in the user with the credential
      const credential = await auth().signInWithCredential(facebookCredential);
      const {profile} = credential.additionalUserInfo;
      const email = credential.user.email
        ? credential.user.email
        : profile?.email
        ? profile.email
        : null;
      const user = {
        id: credential.user.uid,
        firstname: credential.user.displayName ? credential.user.displayName.split(' ')[0] : '',
        lastname: credential.user.displayName ? credential.user.displayName.split(' ')[1] : '',
        fullname: credential?.user?.displayName || '',
        email,
        gender: '',
        birthdate: '',
        device_token: deviceToken,
      };
      const responseUser = await dispatch(getUserAction(deviceToken));
      if (responseUser) {
        return responseUser;
      }
      return dispatch(createAccountAction(user));
    } catch (error) {
      if (error.code && error.code === 'auth/account-exists-with-different-credential') {
        // if the email is already in use, fetch user's email using Facebook's Graph API
        Alert.alert(
          'Tu correo electrónico se creo con una cuenta de Google',
          'Por favor inicia sesion con Google'
        );
      }
      return error;
    }
  };
};
export const signInAppleAction = (deviceToken) => {
  return async (dispatch) => {
    // Start the sign-in request
    const appleAuthRequestResponse = await appleAuth.performRequest({
      requestedOperation: appleAuth.Operation.LOGIN,
      requestedScopes: [appleAuth.Scope.EMAIL, appleAuth.Scope.FULL_NAME],
    });

    // Ensure Apple returned a user identityToken
    if (!appleAuthRequestResponse.identityToken) {
      // Alert.alert('Apple Sign-In failed - no identify token returned');
      // throw 'Apple Sign-In failed - no identify token returned';
      throw appleAuthRequestResponse;
    }

    // Create a Firebase credential from the response
    const {identityToken, nonce} = appleAuthRequestResponse;
    const appleCredential = auth.AppleAuthProvider.credential(identityToken, nonce);
    const credential = auth().signInWithCredential(appleCredential);
    // Sign the user in with the credential
    const user = {
      id: (await credential).user.uid,
      firstname: (await credential).user.displayName
        ? (await credential).user.displayName.split(' ')[0]
        : '',
      lastname: (await credential).user.displayName
        ? (await credential).user.displayName.split(' ')[1]
        : '',
      fullname: (await credential)?.user?.displayName || '',
      email: (await credential).user.email || '',
      gender: '',
      birthdate: '',
      device_token: deviceToken,
    };
    const responseUser = await dispatch(getUserAction(deviceToken));
    if (responseUser) {
      return responseUser;
    }
    return dispatch(createAccountAction(user));
  };
};
