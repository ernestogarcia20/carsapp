import {SET_PATENT_REPORT} from '@constants/reducers';
import {setStore} from '@functions/globals';
import {AppApi} from '@services/index';

export const patentReportFreeAction = (plateNumber) => {
  return async (dispatch) => {
    const {status, data} = await AppApi.GetPatentReportFree(plateNumber);
    if (status !== 200) {
      throw data;
    }
    dispatch(setStore(SET_PATENT_REPORT, data));
    return data;
  };
};

export const getReportAction = (reportId) => {
  return async (dispatch) => {
    const {status, data} = await AppApi.GetReport(reportId);
    if (status !== 200) {
      throw data;
    }
    if (data?.creating) {
      return data;
    }
    dispatch(setStore(SET_PATENT_REPORT, data));
    return data;
  };
};

export const patentReportPremiumAction = (plateNumber) => {
  return async (dispatch) => {
    const {status, data} = await AppApi.GetPatentReportPremium(plateNumber);
    if (status !== 200) {
      throw data;
    }
    if (data?.creating) {
      return data;
    }
    dispatch(setStore(SET_PATENT_REPORT, data));
    return data;
  };
};
export const patentReportQuickAction = (plateNumber) => {
  return async (dispatch) => {
    const {status, data} = await AppApi.GetPatentReportQuick(plateNumber);
    if (status !== 200) {
      throw data;
    }
    dispatch(setStore(SET_PATENT_REPORT, data));
    return data;
  };
};
