import Background from '@images/background.png';
import Splash from '@images/splash.png';
import PatentReport from '@images/patent.png';
import Porcent from '@images/porcent.png';
import Assessment from '@images/assessment.png';
import SearchActive from '@images/search_active.png';
import SearchInactive from '@images/search_inactive.png';
import MyReportsInactive from '@images/my_reports_inactive.png';
import Report from '@images/report.png';
import Info from '@images/Info.png';

const Images = {
  background: Background,
  splash: Splash,
  patent: PatentReport,
  porcent: Porcent,
  assessment: Assessment,
  searchActive: SearchActive,
  myReportsInactive: MyReportsInactive,
  searchInactive: SearchInactive,
  report: Report,
  info: Info,
  /* avatarPlaceholder: require('../img/avatar_placeholder.png'),
  bgGeneral: require('../img/bg_general.png'),
  logo: require('../img/logo_koobai.png'),
  bgOnboard: require('../img/bg_onboard.png'),
  onBoardImg: require('../img/onboard_img.png'),
  iconFb: require('../img/icon_fb.png'),
  iconGoogle: require('../img/icon_google.png'),
  iconApple: require('../img/icon_apple.png'),
  confettis: require('../img/confettis.png'),
  checkImg: require('../img/check_img.png'),
  errorImg: require('../img/error_img.png'),
  iconKoin: require('../img/icon_kooin.png'),
  rewardPlaceholder: require('../img/reward_placeholder.png'),
  iconDatosPerfil: require('../img/icon_datos_perfil.png'),
  iconHistoryActivity: require('../img/icon_historial_actividad.png'),
  iconLogout: require('../img/icon_logout.png'),
  iconTerms: require('../img/icon_terms.png'),
  cameraBtn: require('../img/camera_btn.png'),
  boleta: require('../img/boleta.png'),
  arrowBoleta: require('../img/arrow_boleta.png'),
  clockImage: require('../img/clock_image.png'),
  pdf417: require('../img/pdf417.png'),
  noActivity: require('../img/no_activity.png'),
  marcoScan: require('../img/marco_scan.png'),
  iconAculuma: require('../img/icon_acumula.png'),
  iconAculumaActive: require('../img/icon_acumula_active.png'),
  iconCanjea: require('../img/icon_canjea.png'),
  iconCanjeaActive: require('../img/icon_canjea_active.png'),
  iconMisPremios: require('../img/icon_mis_premios.png'),
  iconMisPremiosActive: require('../img/icon_mis_premios_active.png'),
  giftImage: require('../img/gift.png'),
  iconAcumulaBt: require('../img/icon_acumula_bt.png'),
  iconCanjeaBt: require('../img/icon_canjea_bt.png'),
  iconMisPremiosBt: require('../img/icon_mis_premios_bt.png'), */
};

export default Images;
