import {StyleSheet, Dimensions, Platform, PixelRatio} from 'react-native';
import responseSize from 'react-native-normalize';
import Colors from './Colors';

const {width, height} = Dimensions.get('screen');

const containerStyle = {
  container: {
    backgroundColor: 'transparent',
    elevation: 10000,
    height: responseSize(44),
  },
  statusBar: {
    height: 0,
    backgroundColor: 'white',
  },
};

const styles = StyleSheet.create({
  content: {flex: 1, backgroundColor: Colors.lightBlue},
  topBatNavigation: {
    backgroundColor: 'transparent',
    elevation: 10000,
    height: responseSize(44),
    marginBottom: 16,
    justifyContent: 'center',
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  rowLarge: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  marginRight10: {marginRight: responseSize(10)},
  marginBottom21: {marginBottom: responseSize(21)},
  marginBottom30: {marginBottom: responseSize(30)},
  marginTop35: {marginTop: responseSize(35)},
  marginTop18: {marginTop: responseSize(18)},
  marginBottom18: {marginBottom: responseSize(18)},
  marginBottom10: {marginBottom: responseSize(10)},
  marginBottom5: {marginBottom: responseSize(5)},
  marginBottom8: {marginBottom: responseSize(8)},
});

export {styles, containerStyle};
