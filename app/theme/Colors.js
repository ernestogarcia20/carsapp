const Colors = {
  mainColor: '#FA6325',
  white: '#FFFF',
  lightBlue: '#F3F6F9',
  black: '#000',
  danger: '#FF4047',
  green: '#4CAF50',
  darkLight: '#7E7B7B',
  darkBlue: '#0B2E40',
  blue: '#0096FF',
  mask: 'rgba(0,0,0,0.5)',
};

export default Colors;
