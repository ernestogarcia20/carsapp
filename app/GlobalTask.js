import React, {Component} from 'react';
import {connect} from 'react-redux';
import {BackHandler, View} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import Container from '@components/Container';
import ModalSuscription from '@components/ModalSuscriptions';
import GlobalModal from '@components/GlobalModal';
import {changeInternetAction} from './stores/actions/check-internet.action';
import {styles} from '@assets/styles';
class GlobalTask extends Component {
  componentDidMount() {
    const {_changeInternetAction} = this.props;

    BackHandler.addEventListener('hardwareBackPress', this.backAction);
    NetInfo.fetch().then((e) => {
      _changeInternetAction(e.isConnected);
    });
    this.subscription = NetInfo.addEventListener(this.handleConnectionChange);
  }

  backAction = () => {
    return true;
  };

  handleConnectionChange = (state) => {
    const {_changeInternetAction} = this.props;
    _changeInternetAction(state.isConnected);
  };

  componentWillUnmount() {
    this.subscription && this.subscription();
    BackHandler.removeEventListener('hardwareBackPress', this.backAction);
  }

  render() {
    const {children} = this.props;
    return (
      <Container showStatusBar={false} isOnBoard showBackground>
        <View style={styles.content}>
          <ModalSuscription>
            <GlobalModal />
          </ModalSuscription>
          {children}
          <GlobalModal />
        </View>
      </Container>
    );
  }
}

const mapStateToProps = ({checkInternet, user}) => ({
  checkInternet,
  user,
});

export default connect(mapStateToProps, {_changeInternetAction: changeInternetAction})(GlobalTask);
