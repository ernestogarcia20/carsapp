import React from 'react';
import {connect} from 'react-redux';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import PatentSectionDetailLayout from './components/patent-section-detail-layout';

class PatentSectionDetailController extends BaseComponent {
  constructor(props) {
    super(props);
    const {
      navigation: {getParam},
    } = props;
    const sectionDetail = getParam('sectionDetail', {});
    this.state = {
      title: sectionDetail?.title || '',
      data: sectionDetail?.data || [],
      patentSection: {...sectionDetail, data: null},
    };
  }

  handleInfo = () => {
    const {dispatch} = this.props;
    const {patentSection} = this.state;
    if (!patentSection?.information) {
      return;
    }
    this.setModalGlobal(
      {
        isVisible: true,
        alingDescription: 'left',
        description: patentSection?.information,
      },
      dispatch
    );
  };

  handleChange = (name, value) => this.setState({[name]: value});

  render() {
    const {user} = this.props;
    return (
      <Container>
        <PatentSectionDetailLayout
          goBack={this.goBack}
          setState={this.handleChange}
          user={user}
          handleInfo={this.handleInfo}
          handleGeneratePatentQuickly={this.handleGeneratePatentQuickly}
          handleGeneratePatent={this.handleGeneratePatent}
          state={this.state}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({user}, store) => ({
  dispatch: store.navigation.dispatch,
  user,
});

export default connect(mapStateToProps)(PatentSectionDetailController);
