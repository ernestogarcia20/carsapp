import React from 'react';
import {View, StyleSheet, ScrollView, ActivityIndicator} from 'react-native';
import Header from '@components/Header';
import Text from '@components/Text';
import {styles as Styles} from '@assets/styles';
import Colors from '@assets/Colors';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {TouchableOpacity} from 'react-native-gesture-handler';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentSection: {
    borderRadius: 13,
    shadowColor: 'rgba(0,0,0,0.5)',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    marginTop: '7%',
    backgroundColor: Colors.white,
    paddingBottom: 13,
  },
  contentButtonPremium: {marginTop: 20},
  sectionHeader: {
    paddingTop: 30,
    paddingBottom: 25,
    paddingHorizontal: '7%',
  },
  renderItem: {paddingHorizontal: '9%', paddingBottom: 20, backgroundColor: Colors.white},
  contentContainerStyle: {marginHorizontal: '5%', paddingTop: 5, paddingBottom: 10},
  scrollView: {flex: 1},
  buttonPremium: {paddingBottom: 0, marginTop: 10},
  contentTextItem: {flex: 0.5, justifyContent: 'center'},
  line: {backgroundColor: 'rgba(151, 151, 151, 0.25)', height: 2},
  message: {paddingBottom: 35, paddingTop: 35, paddingHorizontal: 15},
});

const Layout = ({goBack, user, handleReport, state, handleInfo}) => {
  const {data, patentSection} = state;
  const renderItem = ({item, index, length}) => {
    return (
      <TouchableOpacity
        key={index}
        style={styles.renderItem}
        activeOpacity={0.7}
        onPress={() => handleReport(item)}>
        <View style={[Styles.rowLarge]}>
          <View style={styles.contentTextItem}>
            <Text
              color="#0B2E40"
              weight="Heavy"
              style={Styles.marginBottom8}
              align="left"
              fontSize={16}>
              {item?.plate_number || ''}
            </Text>
            <Text color="#0B2E40" weight="Roman" align="left" fontSize={16}>
              {item?.text || ''}
            </Text>
          </View>
        </View>
        {index < length && <View style={[styles.line, Styles.marginTop18, Styles.marginBottom5]} />}
      </TouchableOpacity>
    );
  };

  const renderSectionHeader = (title) => {
    return (
      <View style={styles.sectionHeader}>
        <Text
          style={styles.header}
          fontSize={18}
          weight="Heavy"
          color={Colors.darkBlue}
          align="left">
          {title}
        </Text>
      </View>
    );
  };
  return (
    <View style={styles.container}>
      <Header
        title={state.title}
        info={patentSection?.information}
        onPressInfo={handleInfo}
        onBack={goBack}
      />

      <ScrollView contentContainerStyle={styles.contentContainerStyle} style={styles.scrollView}>
        {typeof data === 'string' ? (
          <View style={[styles.contentSection, Styles.center, styles.message]}>
            <Text weight="Heavy" fontSize={16}>
              {data}
            </Text>
          </View>
        ) : (
          data?.map((section, index) => {
            return (
              <View style={styles.contentSection} key={index}>
                {renderSectionHeader(section.period)}
                <View style={styles.renderItem}>
                  <View style={styles.line} />
                </View>
                {section?.reports.map((data, indexS) => {
                  return renderItem({
                    item: data,
                    index: indexS,
                    length: section?.reports.length - 1,
                  });
                })}
              </View>
            );
          })
        )}
      </ScrollView>
    </View>
  );
};

export default Layout;
