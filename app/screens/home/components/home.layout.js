import React from 'react';
import Header from '@components/Header';
import {View, StyleSheet, FlatList, TouchableOpacity} from 'react-native';
import Text from '@components/Text';
import ButtonPremium from '@components/ButtonPremium';
import Colors from '@assets/Colors';
import FastImage from 'react-native-fast-image';
import Images from '@assets/Images';
import responseSize from 'react-native-normalize';

const styles = StyleSheet.create({
  contentButtonPremium: {justifyContent: 'flex-end', alignItems: 'center', flex: 0.2},
  contentItem: {
    flexDirection: 'row',
    height: responseSize(74),
    backgroundColor: 'white',
    marginHorizontal: '6%',
    paddingHorizontal: responseSize(20),
    alignItems: 'center',
    borderRadius: 13,
    marginBottom: responseSize(30),
    shadowColor: 'rgba(0,0,0,0.3)',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 3,
  },
  contentIcon: {flex: 0.15},
  contentText: {flex: 0.9},
});

const Layout = ({setState, user, handleShowModalPrices, state, handlePressItem}) => {
  const {buttons} = state;

  const renderItem = ({item}) => {
    const {text, icon, height, width} = item;

    return (
      <TouchableOpacity
        activeOpacity={0.7}
        style={styles.contentItem}
        onPress={() => handlePressItem(item)}>
        <View style={styles.contentIcon}>
          <FastImage
            source={Images[icon]}
            style={{width: responseSize(width), height: responseSize(height)}}
          />
        </View>
        <View style={styles.contentText}>
          <Text align="left" fontSize={16} color="#0B2E40">
            {text}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };
  return (
    <>
      <Header
        title="AUTODATO"
        isProfile
        color="#0B2E40"
        onPressProfile={() => setState('showModal', true)}
      />
      <FlatList
        scrollEnabled={false}
        data={buttons}
        renderItem={renderItem}
        contentContainerStyle={{flex: 1, justifyContent: 'center', marginTop: '20%'}}
        keyExtractor={(item, key) => item + key}
      />
      <View style={styles.contentButtonPremium}>
        <ButtonPremium onPressProfile={() => setState('showModal', true)} />
      </View>
    </>
  );
};

export default Layout;
