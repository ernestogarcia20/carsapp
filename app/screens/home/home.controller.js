import React from 'react';
import {connect} from 'react-redux';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import ModalProfile from '@components/ModalProfile';
import OneSignal from 'react-native-onesignal';
import {homeAction} from '@actions/home.actions';
import {getReportAction} from '@actions/patent-report.actions';
import {setDeviceTokenStoreAction} from '@actions/auth.actions';
import {NAVIGATE_PATENT_REPORT, NAVIGATE_PATENT_PREMIUM_NAV, NAVIGATE_REPORT_DETAIL} from '@constants/navigations';
import {Platform, Alert} from 'react-native';
import config from '../../config';
import HomeLayout from './components/home.layout';

class HomeController extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
      showModalPrices: false,
      buttons: [
        {
          text: 'Informe por patente',
          navigate: NAVIGATE_PATENT_REPORT,
          icon: 'patent',
          width: 20,
          height: 20,
        },
        {
          text: 'Tasación fiscal y permiso de circulación',
          navigate: '',
          icon: 'assessment',
          width: 20,
          height: 20,
        },
      ],
    };
  }

  async componentDidMount() {
    OneSignal.setAppId(config.oneSignalAppId);
    OneSignal.setLogLevel(6, 0);
    OneSignal.setRequiresUserPrivacyConsent(false);
    /* OneSignal.setNotificationWillShowInForegroundHandler((notifReceivedEvent) => {
      console.log('OneSignal: notification will show in foreground:', notifReceivedEvent);
      const notif = notifReceivedEvent.getNotification();

      const button1 = {
        text: 'Cancel',
        onPress: () => {
          notifReceivedEvent.complete();
        },
        style: 'cancel',
      };

      const button2 = {
        text: 'Complete',
        onPress: () => {
          notifReceivedEvent.complete(notif);
        },
      };

      Alert.alert('Complete notification?', 'Test', [button1, button2], {cancelable: true});
    }); */
    OneSignal.setNotificationWillShowInForegroundHandler(this.onReceived);
    OneSignal.setNotificationOpenedHandler(this.onOpened);
    if (Platform.OS === 'android') {
      this.onIds(true);
    } else {
      OneSignal.promptForPushNotificationsWithUserResponse(this.onIds);
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.user?.id !== this.props.user?.id) {
      this.init();
    }
  }

  init = () => {
    if (!this.props.user) {
    }
    // this.componentDidMount();
  };

  GetDataHome = async () => {
    const {_homeAction} = this.props;

    try {
      const data = await _homeAction(this.state.deviceToken);
    } catch (e) {}
  };

  onIds = async (device) => {
    const {_setDeviceTokenStoreAction, user} = this.props;
    if (!device) {
      return;
    }
    setTimeout(async () => {
      const deviceState = await OneSignal.getDeviceState();
      await _setDeviceTokenStoreAction(deviceState.userId || null);
      if (!user || !deviceState.userId) {
        return;
      }
      this.setState({deviceToken: deviceState.userId});
      this.GetDataHome();
    }, 400);
  };

  onReceived = (notification) => {
    /* if (notification.isAppInFocus && Platform.OS === 'android') {
      this.setState({
        remoteMessage: notification.payload,
        showNotification: true,
      });
    } */
  };

  onOpened = async (openResult) => {
    console.log('OPENED', openResult);
    const reportId = openResult?.notification?.additionalData?.report_id;
    if (!reportId) {
      return;
    }
    const {_getReportAction} = this.props;
    try {
      await _getReportAction('202103cddc35');
      this.navigate(NAVIGATE_REPORT_DETAIL);
      this.GetDataHome();
    } catch (e) {
      Alert.alert('No se pudo abrir el reporte', 'Vuelve a intentarlo más tarde.');
    }
    // this.GotoBillSummary(openResult.notification.payload.additionalData.bill_id);
  };

  handleShowModalPrices = () => {
    this.setState({showModalPrices: true});
  };

  handlePressItem = ({navigate}) => {
    if (!navigate) {
      return;
    }

    const {user} = this.props;
    let newNavigate = navigate;

    if (newNavigate === NAVIGATE_PATENT_REPORT && user?.is_premium) {
      newNavigate = NAVIGATE_PATENT_PREMIUM_NAV;
    }

    this.navigate(newNavigate);
  };

  handleChange = (name, value) => this.setState({[name]: value});

  render() {
    return (
      <ModalProfile
        setState={(showModal) => {
          this.handleChange('showModal', showModal);
        }}
        showModal={this.state.showModal}>
        <Container>
          <HomeLayout
            user={this.props.user}
            goBack={this.goBack}
            dispatch={this.props.dispatch}
            setState={this.handleChange}
            state={this.state}
            handlePressItem={this.handlePressItem}
            handleShowModalPrices={this.handleShowModalPrices}
          />
        </Container>
      </ModalProfile>
    );
  }
}

const mapStateToProps = ({user}, store) => ({
  dispatch: store.navigation.dispatch,
  user,
});

export default connect(mapStateToProps, {
  _homeAction: homeAction,
  _setDeviceTokenStoreAction: setDeviceTokenStoreAction,
  _getReportAction: getReportAction,
})(HomeController);
