import React, {useState, useEffect} from 'react';
import {View, StyleSheet, TouchableOpacity, SafeAreaView} from 'react-native';
import Swiper from 'react-native-swiper';
import Colors from '@assets/Colors';
import Text from '@components/Text';
import FastImage from 'react-native-fast-image';
import ReponseSize from 'react-native-normalize';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  onboardImg: {
    width: ReponseSize(178),
    height: ReponseSize(174),
  },
  swipe: {flex: 1},
  center: {justifyContent: 'center', alignItems: 'center'},
  text: {
    lineHeight: 38,
    fontSize: 26,
  },
  contentImage: {paddingTop: 180, flex: 0.5, justifyContent: 'flex-end'},
  contentText: {paddingTop: 60, paddingHorizontal: 40, flex: 0.5},
  dotActive: {
    backgroundColor: Colors.white,
    width: 10,
    height: 10,
    borderRadius: 100,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 3,
    marginBottom: 3,
  },
  dotInActive: {
    backgroundColor: 'rgba(255,255,255,0.5)',
    width: 10,
    height: 10,
    borderRadius: 100,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 3,
    marginBottom: 3,
  },
  contentSkip: {
    position: 'absolute',
    right: '5%',
    top: '5%',
    zIndex: 1,
  },
});

const Layout = ({onboardSlides, handleSkip}) => {
  const dotActive = () => {
    return <View style={styles.dotActive} />;
  };
  const dotInactive = () => {
    return <View style={styles.dotInActive} />;
  };
  return (
    <View style={styles.container}>
      <Swiper
        showsPagination
        activeDotColor={Colors.white}
        dot={dotInactive()}
        activeDot={dotActive()}
        loop={false}>
        {onboardSlides.length > 0 &&
          onboardSlides.map((slide, index) => {
            return (
              <View style={styles.swipe} key={index}>
                <SafeAreaView style={[styles.contentSkip]}>
                  <TouchableOpacity onPress={handleSkip}>
                    <Text color={Colors.white} weight="Heavy" fontSize={16}>
                      Omitir
                    </Text>
                  </TouchableOpacity>
                </SafeAreaView>
                <View style={styles.contentImage}>
                  <View style={[styles.center]}>
                    <FastImage
                      resizeMode="contain"
                      style={styles.onboardImg}
                      source={{uri: slide.image_url, priority: 'high'}}
                    />
                  </View>
                </View>
                <View style={styles.contentText}>
                  <View>
                    <Text color={Colors.white} style={styles.text}>
                      {slide.text}
                    </Text>
                  </View>
                </View>
              </View>
            );
          })}
      </Swiper>
    </View>
  );
};

export default Layout;
