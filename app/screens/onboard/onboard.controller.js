import React from 'react';
import {connect} from 'react-redux';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import {NAVIGATE_HOME} from '@constants/navigations';
import OnboardLayout from './components/onboard.layout';

class OnboardController extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  handleSkip = () => {
    this.navigate(NAVIGATE_HOME);
  };

  handleChange = (name, value) => this.setState({[name]: value});

  render() {
    const {onboardSlides} = this.props;
    return (
      <Container isOnBoard showStatusBar={false} showBackground>
        <OnboardLayout
          goBack={this.goBack}
          handleSkip={this.handleSkip}
          onboardSlides={onboardSlides}
          setState={this.handleChange}
          state={this.state}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({user, onBoard: {onboardSlides}}, store) => ({
  dispatch: store.navigation.dispatch,
  user,
  onboardSlides,
});

export default connect(mapStateToProps)(OnboardController);
