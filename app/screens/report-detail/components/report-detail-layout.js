import React from 'react';
import {View, StyleSheet, ScrollView, TouchableOpacity} from 'react-native';
import Header from '@components/Header';
import Text from '@components/Text';
import ButtonPremium from '@components/ButtonPremium';
import Colors from '@assets/Colors';
import {styles as Styles} from '@assets/styles';
import Icon from 'react-native-vector-icons/FontAwesome5';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentSection: {
    borderRadius: 13,
    shadowColor: 'rgba(0,0,0,0.5)',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    marginTop: '7%',
    backgroundColor: Colors.white,
    paddingBottom: 13,
  },
  contentButtonPremium: {marginTop: 20},
  sectionHeader: {
    paddingTop: 30,
    paddingBottom: 25,
    paddingHorizontal: '7%',
  },
  renderItem: {paddingHorizontal: '7%', paddingBottom: 20},
  contentContainerStyle: {marginHorizontal: '5%', paddingTop: 5, paddingBottom: 10},
  scrollView: {flex: 1},
  buttonPremium: {paddingBottom: 0, marginTop: 10},
  contentTextItem: {flex: 0.5, justifyContent: 'center'},
  contentTypes: {
    justifyContent: 'flex-end',
    paddingHorizontal: '7%',
    paddingBottom: 10,
    paddingTop: 10,
  },
});

const Layout = ({
  goBack,
  state,
  setState,
  user,
  handleInfo,
  patentReport,
  handlePatentSectionDetail,
}) => {
  const {reportList} = state;

  const renderItem = ({item, index}) => {
    return (
      <View style={[Styles.rowLarge, styles.renderItem]} key={index}>
        <View style={styles.contentTextItem}>
          <Text color="#7E7B7B" weight="Heavy" align="left" fontSize={16}>
            {item?.label || ''}
          </Text>
        </View>
        <View style={styles.contentTextItem}>
          <Text
            color={item?.value_color || Colors.darkBlue}
            align="right"
            weight="Heavy"
            fontSize={16}>
            {item?.value || ''}
          </Text>
        </View>
      </View>
    );
  };

  const renderSectionHeader = (title, index) => {
    return (
      <View style={styles.sectionHeader} key={index}>
        <Text
          style={styles.header}
          fontSize={18}
          weight="Heavy"
          color={Colors.darkBlue}
          align="left">
          {title}
        </Text>
      </View>
    );
  };
  return (
    <View style={styles.container}>
      <Header
        title="Informe"
        onBack={goBack}
        info={patentReport?.information}
        onPressInfo={handleInfo}
      />
      <ScrollView contentContainerStyle={styles.contentContainerStyle} style={styles.scrollView}>
        {reportList?.map((section, index) => {
          return (
            <View style={styles.contentSection} key={index}>
              {renderSectionHeader(section.title, index)}
              {section?.data?.map((data, indexS) => {
                return renderItem({item: data, index: indexS});
              })}
              {section?.type === 'button' && (
                <TouchableOpacity
                  style={[Styles.row, styles.contentTypes]}
                  activeOpacity={0.65}
                  onPress={() =>
                    handlePatentSectionDetail({title: section.title, ...section.inner_data})
                  }>
                  <View style={{marginRight: 15}}>
                    <Text color={Colors.blue} fontSize={16} weight="Heavy">
                      {section?.button_text || 'Ver Detalles'}
                    </Text>
                  </View>
                  <Icon name="play" size={9} color={Colors.blue} />
                </TouchableOpacity>
              )}
            </View>
          );
        })}
        {!user?.is_premium && (
          <View style={[styles.contentButtonPremium, Styles.center]}>
            <ButtonPremium
              onPressProfile={() => setState('showModal', true)}
              style={styles.buttonPremium}
              showTextPremium={false}
            />
          </View>
        )}
      </ScrollView>
    </View>
  );
};

export default Layout;
