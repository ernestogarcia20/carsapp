import React from 'react';
import {connect} from 'react-redux';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import ModalProfile from '@components/ModalProfile';
import {NAVIGATE_PATENT_SECTION_DETAIL} from '@constants/navigations';
import ReportDetailLayout from './components/report-detail-layout';

class ReportDetailController extends BaseComponent {
  constructor(props) {
    super(props);
    const {patentReport} = props;
    this.state = {
      showModal: false,
      patent: 'CDDC35',
      loading: false,
      reportList: patentReport?.boxes || [],
    };
  }

  handleInfo = () => {
    const {patentReport, dispatch} = this.props;
    if (!patentReport?.information) {
      return;
    }
    this.setModalGlobal(
      {
        isVisible: true,
        alingDescription: 'left',
        description: patentReport?.information,
      },
      dispatch
    );
  };

  handlePatentSectionDetail = (sectionDetail) => {
    this.navigate(NAVIGATE_PATENT_SECTION_DETAIL, {sectionDetail});
  };

  handleChange = (name, value) => this.setState({[name]: value});

  render() {
    return (
      <ModalProfile
        setState={(showModal) => {
          this.handleChange('showModal', showModal);
        }}
        showModal={this.state.showModal}>
        <Container>
          <ReportDetailLayout
            goBack={this.goBack}
            setState={this.handleChange}
            handleInfo={this.handleInfo}
            handlePatentSectionDetail={this.handlePatentSectionDetail}
            user={this.props.user}
            patentReport={this.props.patentReport}
            handleGeneratePatent={this.handleGeneratePatent}
            state={this.state}
          />
        </Container>
      </ModalProfile>
    );
  }
}

const mapStateToProps = ({user, patentReport: {patentReport}}, store) => ({
  dispatch: store.navigation.dispatch,
  user,
  patentReport,
});

export default connect(mapStateToProps)(ReportDetailController);
