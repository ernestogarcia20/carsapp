import React from 'react';
import {connect} from 'react-redux';
import {getOnBoardAction, getUserAction} from '@actions/auth.actions';
import {setDispatchAction} from '@actions/dispatch.actions';
import {
  setModalSuscriptionAction,
} from '@actions/subscriptions.actions';
import BaseComponent from '@components/BaseComponent';
import SplashScreen from 'react-native-splash-screen';
import {NAVIGATE_HOME, NAVIGATE_ONBOARD} from '@constants/navigations';
import SplashLayout from './components/splash.layout';

class SplashController extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    setTimeout(() => {
      this.init();
      SplashScreen.hide();
    }, 400);
  }

  init = async () => {
    const {
      user,
      onboardSlides,
      _getOnBoardAction,
      _setDispatchAction,
      dispatch,
      _getUserAction,
      deviceToken,
      _setModalSuscriptionAction,
    } = this.props;
    _setModalSuscriptionAction(false);
    _setDispatchAction(dispatch);
    if (user) {
      await _getUserAction(deviceToken);
      this.navigate(NAVIGATE_HOME);
      setTimeout(() => {
        // SplashScreen.hide();
      }, 300);
      return;
    }
    if (onboardSlides.length > 0) {
      this.navigate(NAVIGATE_ONBOARD);
      setTimeout(() => {
        // SplashScreen.hide();
      }, 300);
      return;
    }
    try {
      await _getOnBoardAction();
      this.navigate(NAVIGATE_ONBOARD);
      setTimeout(() => {
        // SplashScreen.hide();
      }, 300);
    } catch (e) {}
  };

  handleChange = (name, value) => this.setState({[name]: value});

  render() {
    return <SplashLayout goBack={this.goBack} setState={this.handleChange} state={this.state} />;
  }
}

const mapStateToProps = ({user, onBoard: {onboardSlides}, deviceToken}, store) => ({
  dispatch: store.navigation.dispatch,
  user,
  deviceToken,
  onboardSlides,
});

export default connect(mapStateToProps, {
  _getOnBoardAction: getOnBoardAction,
  _setDispatchAction: setDispatchAction,
  _getUserAction: getUserAction,
  _setModalSuscriptionAction: setModalSuscriptionAction,
})(SplashController);
