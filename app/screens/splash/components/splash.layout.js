import React from 'react';
import {View, StyleSheet, ActivityIndicator} from 'react-native';
import FastImage from 'react-native-fast-image';
import Images from '@assets/Images';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#C4C4C4',
  },
});

const Layout = ({goBack}) => {
  return (
    <View style={styles.container}>
      <FastImage source={Images.splash} style={{flex: 1}}>
        <View style={{flex: 0.65, justifyContent: 'flex-end', alignItems: 'center'}}>
          <ActivityIndicator color="white" size="small" />
        </View>
      </FastImage>
    </View>
  );
};

export default Layout;
