import React from 'react';
import {connect} from 'react-redux';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import {patentReportFreeAction} from '@actions/patent-report.actions';
import ModalProfile from '@components/ModalProfile';
import {Alert} from 'react-native';
import {
  NAVIGATE_REPORT_DETAIL,
  NAVIGATE_HOME,
  NAVIGATE_PATENT_PREMIUM_NAV,
} from '@constants/navigations';
import PatentReportLayout from './components/patent-report-layout';

class PatentReportController extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
      patent: 'CDDC35',
      loading: false,
    };
  }

  componentDidUpdate(prevProps) {
    const {user} = this.props;
    if (!user) {
      return;
    }
    if (user.is_premium !== prevProps.user.is_premium) {
      this.changeToPatentPremium();
    }
  }

  changeToPatentPremium = () => {
    const {user} = this.props;
    if (!user.is_premium) {
      return;
    }
    this.goBack();
    this.navigate(NAVIGATE_PATENT_PREMIUM_NAV);
  };

  handleGeneratePatent = async () => {
    const {_patentReportFreeAction} = this.props;
    const {patent} = this.state;
    if (!patent) {
      return;
    }
    this.setState({loading: true});
    try {
      await _patentReportFreeAction(patent);
      this.navigate(NAVIGATE_REPORT_DETAIL);
      this.setState({loading: false});
    } catch (e) {
      this.setState({loading: false});
      Alert.alert('No se obtuvieron resultados');
    }
  };

  handleChange = (name, value) => this.setState({[name]: value});

  render() {
    return (
      <ModalProfile
        setState={(showModal) => {
          this.handleChange('showModal', showModal);
        }}
        showModal={this.state.showModal}>
        <Container>
          <PatentReportLayout
            goBack={() => this.navigate(NAVIGATE_HOME)}
            setState={this.handleChange}
            handleGeneratePatent={this.handleGeneratePatent}
            state={this.state}
          />
        </Container>
      </ModalProfile>
    );
  }
}

const mapStateToProps = ({user}, store) => ({
  dispatch: store.navigation.dispatch,
  user,
});

export default connect(mapStateToProps, {_patentReportFreeAction: patentReportFreeAction})(
  PatentReportController
);
