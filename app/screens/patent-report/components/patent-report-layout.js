import React from 'react';
import {View, StyleSheet, KeyboardAvoidingView} from 'react-native';
import Header from '@components/Header';
import ButtonPremium from '@components/ButtonPremium';
import Button from '@components/Button';
import Input from '@components/Input';
import ResponsiveSize from 'react-native-normalize';
import {styles as Styles} from '@assets/styles';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {flex: 1, justifyContent: 'center', paddingTop: '10%'},
  contentInput: {marginHorizontal: '7%', marginBottom: ResponsiveSize(56)},
  contentButtonPremium: {justifyContent: 'flex-end', alignItems: 'center', flex: 0.1},
});

const Layout = ({setState, goBack, handleGeneratePatent, state}) => {
  const {patent, loading} = state;
  return (
    <View style={styles.container} pointerEvents={loading ? 'none' : 'auto'}>
      <Header title="Informe por patente" onBack={goBack} />
      <KeyboardAvoidingView style={styles.container} enabled behavior="padding">
        <View style={styles.content}>
          <View style={styles.contentInput}>
            <Input
              text="Patente"
              textColor="#7E7B7B"
              attrName="patent"
              value={patent}
              updateMasterState={setState}
            />
          </View>
          <View style={Styles.center}>
            <Button text="Generar informe" onPress={handleGeneratePatent} loading={loading} />
          </View>
        </View>
      </KeyboardAvoidingView>
      <View style={styles.contentButtonPremium}>
        <ButtonPremium onPressProfile={() => setState('showModal', true)} showTextPremium={false} />
      </View>
    </View>
  );
};

export default Layout;
