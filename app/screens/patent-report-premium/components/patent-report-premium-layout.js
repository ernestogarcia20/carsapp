import React from 'react';
import {View, StyleSheet, KeyboardAvoidingView} from 'react-native';
import Header from '@components/Header';
import ButtonPremium from '@components/ButtonPremium';
import Button from '@components/Button';
import Input from '@components/Input';
import ResponsiveSize from 'react-native-normalize';
import {styles as Styles} from '@assets/styles';
import Colors from '@assets/Colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {flex: 1, justifyContent: 'center', paddingTop: '10%'},
  contentInput: {marginHorizontal: '7%', marginBottom: ResponsiveSize(56)},
  contentButtonPremium: {justifyContent: 'flex-end', alignItems: 'center', flex: 0.1},
});

const Layout = ({goBack, setState, handleGeneratePatent, handleGeneratePatentQuickly, state}) => {
  const {patent, loading, loadingReportFast} = state;
  return (
    <View style={styles.container} pointerEvents={loading || loadingReportFast ? 'none' : 'auto'}>
      <Header title="Informe por patente" onBack={goBack} />
      <KeyboardAvoidingView style={styles.container} enabled behavior="padding">
        <View style={styles.content}>
          <View style={styles.contentInput}>
            <Input
              text="Patente"
              textColor="#7E7B7B"
              attrName="patent"
              value={patent}
              updateMasterState={setState}
            />
          </View>
          <View style={Styles.center}>
            <Button text="Generar informe" onPress={handleGeneratePatent} loading={loading} />
          </View>
          <View style={[Styles.center, Styles.marginTop35]}>
            <Button
              text="Informe rápido"
              textColor={Colors.darkLight}
              buttonColor={Colors.white}
              onPress={handleGeneratePatentQuickly}
              loading={loadingReportFast}
            />
          </View>
        </View>
      </KeyboardAvoidingView>
    </View>
  );
};

export default Layout;
