import React from 'react';
import {connect} from 'react-redux';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import {patentReportPremiumAction, patentReportQuickAction} from '@actions/patent-report.actions';
import ModalProfile from '@components/ModalProfile';
import {Alert, Keyboard} from 'react-native';
import {NAVIGATE_REPORT_DETAIL, NAVIGATE_HOME} from '@constants/navigations';
import Images from '@assets/Images';
import PatentReportLayout from './components/patent-report-premium-layout';

class PatentReportController extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
      patent: 'CDDC35',
      loading: false,
      loadingReportFast: false,
    };
  }

  handleGeneratePatent = async () => {
    Keyboard.dismiss();
    const {_patentReportPremiumAction} = this.props;
    const {patent} = this.state;
    if (!patent) {
      return;
    }
    this.setState({loading: true});
    try {
      const data = await _patentReportPremiumAction(patent);
      if (data?.creating) {
        this.showModalCreating();
      } else {
        this.navigate(NAVIGATE_REPORT_DETAIL);
      }
      console.log('REPORT PREMIUM', JSON.stringify(data));
      this.setState({loading: false});
    } catch (e) {
      this.setState({loading: false});
      Alert.alert('No se obtuvieron resultados');
    }
  };

  showModalCreating = () => {
    const {dispatch} = this.props;
    this.setModalGlobal(
      {
        isVisible: true,
        isActionSheet: false,
        title: 'Preparando informe',
        description: `Estamos preparando el informe de la patente ${this.state.patent} \n\n En breves minutos recibirás una notificación`,
        image: Images.report,
      },
      dispatch
    );
    this.setState({patent: ''});
  };

  handleGeneratePatentQuickly = async () => {
    Keyboard.dismiss();
    const {_patentReportQuickAction} = this.props;
    const {patent} = this.state;
    if (!patent) {
      return;
    }
    this.setState({loadingReportFast: true});
    try {
      await _patentReportQuickAction(patent);
      this.navigate(NAVIGATE_REPORT_DETAIL);

      this.setState({loadingReportFast: false});
    } catch (e) {
      this.setState({loadingReportFast: false});
      Alert.alert('No se obtuvieron resultados');
    }
  };

  onGoBack = () => {
    this.navigate(NAVIGATE_HOME);
  };

  handleChange = (name, value) => this.setState({[name]: value});

  render() {
    const {user} = this.props;
    return (
      <ModalProfile
        setState={(showModal) => {
          this.handleChange('showModal', showModal);
        }}
        showModal={this.state.showModal}>
        <Container>
          <PatentReportLayout
            goBack={this.onGoBack}
            setState={this.handleChange}
            user={user}
            handleGeneratePatentQuickly={this.handleGeneratePatentQuickly}
            handleGeneratePatent={this.handleGeneratePatent}
            state={this.state}
          />
        </Container>
      </ModalProfile>
    );
  }
}

const mapStateToProps = ({user}, store) => ({
  dispatch: store.navigation.dispatch,
  user,
});

export default connect(mapStateToProps, {
  _patentReportQuickAction: patentReportQuickAction,
  _patentReportPremiumAction: patentReportPremiumAction,
})(PatentReportController);
