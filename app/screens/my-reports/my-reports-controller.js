import React from 'react';
import {connect} from 'react-redux';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import {getReportAction} from '@actions/patent-report.actions';
import {NAVIGATE_REPORT_DETAIL, NAVIGATE_HOME} from '@constants/navigations';
import {Alert} from 'react-native';
import MyReportsLayout from './components/my-reports-layout';

class MyReportsController extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      loadingByReport: null,
    };
  }

  handleReport = async (report) => {
    const {_getReportAction} = this.props;
    this.setState({loadingByReport: report.id});
    try {
      await _getReportAction(report.id);
      this.navigate(NAVIGATE_REPORT_DETAIL);
      this.setState({loadingByReport: null});
    } catch (e) {
      Alert.alert('No se pudo abrir el reporte', 'Vuelve a intentarlo más tarde.');
      this.setState({loadingByReport: null});
    }
  };

  onGoBack = () => {
    this.navigate(NAVIGATE_HOME);
  };

  handleChange = (name, value) => this.setState({[name]: value});

  render() {
    return (
      <Container>
        <MyReportsLayout
          handleReport={this.handleReport}
          goBack={this.onGoBack}
          user={this.props.user}
          setState={this.handleChange}
          state={this.state}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({user}, store) => ({
  dispatch: store.navigation.dispatch,
  user,
});

export default connect(mapStateToProps, {_getReportAction: getReportAction})(MyReportsController);
