import React, {FunctionComponent} from 'react';
import {StyleSheet, Text, ViewPropTypes} from 'react-native';
import Colors from '@assets/Colors';
import PropTypes from 'prop-types';
import responsiveSize from 'react-native-normalize';
import {
  FontRoman,
  FontHeavy,
  FontLight,
  FontBlack,
  FontMedium,
  FontRegular,
  FontItalic,
  FontThin,
} from '@constants/fonts';
import {fontMaker} from './typografy';

declare type Props = {
  align?: 'auto' | 'left' | 'right' | 'center' | 'justify',
  weight?: 'Roman' | 'Heavy' | 'Light' | 'Black' | 'Medium' | 'Regular' | 'Italic' | 'Thin',
  fontSize: number,
};

const TextComponent: FunctionComponent<Props> = ({
  children,
  fontSize,
  color,
  weight,
  align,
  style,
}) => {
  const styles = StyleSheet.create({
    text: {
      fontSize: responsiveSize(fontSize),
      color,
      textAlign: align,
      ...fontMaker(weight),
    },
  });
  return <Text style={[styles.text, style]}>{children}</Text>;
};

TextComponent.propTypes = {
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]).isRequired,
  style: ViewPropTypes.style,
  fontSize: PropTypes.number,
  color: PropTypes.string,
  weight: PropTypes.oneOf([
    FontRoman,
    FontHeavy,
    FontLight,
    FontBlack,
    FontMedium,
    FontRegular,
    FontItalic,
    FontThin,
  ]),
  align: PropTypes.oneOf(['auto', 'left', 'right', 'center', 'justify']),
};

TextComponent.defaultProps = {
  style: ViewPropTypes.style,
  color: Colors.BLACK,
  fontSize: 14,
  weight: 'Roman',
  align: 'center',
};

export default TextComponent;
