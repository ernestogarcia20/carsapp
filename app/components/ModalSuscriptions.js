import React, {useState, useEffect} from 'react';
import {
  TouchableOpacity,
  View,
  StyleSheet,
  Modal,
  Platform,
  ActivityIndicator,
  TouchableWithoutFeedback,
  Alert,
} from 'react-native';
import {connect} from 'react-redux';
import Text from '@components/Text';
import Colors from '@assets/Colors';
import Icon from 'react-native-vector-icons/Feather';
import {
  suscriptionAction,
  setModalSuscriptionAction,
  checkSuscriptionAction,
  moveSuscriptionAction,
} from '@actions/subscriptions.actions';
import {setModalAction} from '@actions/modal.actions';
import {styles as Styles} from '@assets/styles';

import RNIap, {
  InAppPurchase,
  PurchaseError,
  SubscriptionPurchase,
  acknowledgePurchaseAndroid,
  consumePurchaseAndroid,
  finishTransaction,
  processNewPurchase,
  purchaseErrorListener,
  purchaseUpdatedListener,
} from 'react-native-iap';
import responsiveSize from 'react-native-normalize';
import {FontBlack} from '@constants/fonts';

const styles = StyleSheet.create({
  contentButtonSubscription: {
    justifyContent: 'flex-end',
    paddingBottom: '5%',
  },
  center: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonPremium: {
    backgroundColor: Colors.danger,
    width: responsiveSize(176),
    height: responsiveSize(44),
    borderRadius: 22,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 10,
  },
  contentModal: {
    marginHorizontal: responsiveSize(25),
    backgroundColor: Colors.lightBlue,
    borderRadius: 20,
    paddingTop: responsiveSize(45),
    paddingHorizontal: responsiveSize(30),
    paddingVertical: 10,
    shadowColor: Colors.black,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  maskModal: {flex: 1, backgroundColor: Colors.mask, justifyContent: 'center'},
  contentBenefits: {paddingRight: '13%', paddingVertical: '10%', paddingTop: '10%'},
  buttonClose: {
    backgroundColor: Colors.white,
    width: responsiveSize(139),
    height: responsiveSize(45),
    borderRadius: 22,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 10,
  },
  buttonPrice: {
    borderRadius: 22,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 10,
    height: responsiveSize(45),
    width: responsiveSize(205),
    backgroundColor: Colors.blue,
    ...Styles.center,
  },
});

const subsIds = Platform.select({
  ios: ['iap_monthly_subscription', 'iap_yearly_subscription'],
  android: ['iap_monthly_subscription', 'iap_yearly_subscription'],
});

const benefitsList = [
  'Informes completos: historial de revisión técnica, multas y otros',
  'Informes ilimitados',
  'Acceso a valor de mercado',
];

const subscriptionsType = {
  MONTH: '/ mes',
  YEAR: '/ año',
};

/* const buttonPrices = [
  {title: '$ 2.900 / mes', sku: 'iap_monthly_subscription'},
  {title: '$ 24.900 / año', sku: 'iap_yearly_subscription'},
]; */

let purchaseUpdateSubscription;
let purchaseErrorSubscription;
let renewal = true;

const ButtonPremium = ({user, dispatch, showModalSuscription, children, deviceToken}) => {
  const [skuSelected, setSkuSelected] = useState(null);
  const [buttonPrices, setButtonPrices] = useState([]);
  const [showModalSuccess, setShowModalSuccess] = useState(false);

  useEffect(() => {
    if (showModalSuscription) {
      setShowModalSuccess(false);
    }
  }, [showModalSuscription]);

  const purchaseConfirmed = async (purchase) => {
    if (Platform.OS === 'ios') {
      await RNIap.finishTransactionIOS(purchase.transactionId);
      await RNIap.clearTransactionIOS();
    } else if (Platform.OS === 'android') {
      await RNIap.acknowledgePurchaseAndroid(purchase.purchaseToken);
    }
    await RNIap.finishTransaction(purchase, false);
  };

  const changeSubscription = async (data, purchase) => {
    // console.log('CHANGE SUB', data);
    try {
      const result = await dispatch(moveSuscriptionAction(data, deviceToken));
      setTimeout(() => {
        setSkuSelected(null);
        setShowModalSuccess(true);
      }, 250);

      purchaseConfirmed(purchase);
    } catch (e) {
      setSkuSelected(null);
      console.warn('modale', e);
    }
  };

  const init = async () => {
    try {
      await RNIap.initConnection();
      if (buttonPrices.length === 0) {
        const subscriptions = await RNIap.getSubscriptions(subsIds);
        setButtonPrices(
          subscriptions.map((sub) => {
            return {
              title: `${sub.localizedPrice} ${subscriptionsType[sub.subscriptionPeriodUnitIOS]}`,
              sku: sub.productId,
            };
          })
        );
      }
    } catch (err) {
      setSkuSelected(null);
      console.log('error in cdm => ', err);
    }
    purchaseUpdateSubscription = purchaseUpdatedListener(
      async (purchase: InAppPurchase | SubscriptionPurchase) => {
        const receipt = purchase.transactionReceipt;

        if (receipt) {
          if (renewal) {
            purchaseConfirmed(purchase);
            return;
          }
          renewal = true;
          try {
            const data = {
              user_id: user?.id,
              receipt_data: receipt,
              original_transaction_id: purchase.originalTransactionIdentifierIOS,
            };
            const checkSubscription = await dispatch(
              checkSuscriptionAction(data.original_transaction_id)
            );
            if (checkSubscription) {
              dispatch(
                setModalAction({
                  isVisible: true,
                  isActionSheet: true,
                  description: `Ya existe otro usuario con subscripción pagada con esta cuenta de ${
                    Platform.OS === 'android' ? 'Google Play' : 'Apple'
                  }.\n¿Desea traspasar la subscripción a este usuario?`,
                  onConfirm: () => changeSubscription({...data, receipt_data: null}, purchase),
                  onCancel: async () => {
                    setSkuSelected(null);
                    setShowModalSuccess(false);
                    purchaseConfirmed(purchase);
                  },
                })
              );
            } else {
              const result = await dispatch(suscriptionAction(data));
              setSkuSelected(null);
              setShowModalSuccess(true);
              purchaseConfirmed(purchase);
            }
          } catch (ackErr) {
            setSkuSelected(null);
            setShowModalSuccess(false);
            console.warn('ackErr', ackErr);
          }
        }
      }
    );

    purchaseErrorSubscription = purchaseErrorListener((error: PurchaseError) => {
      if (error.code !== 'E_USER_CANCELLED' && error.code !== 'E_UNKNOWN') {
        Alert.alert('No se pudo procesar el pago');
      }
      console.warn('ackErr2', error);
      setSkuSelected(null);
    });
  };

  const disconnect = () => {
    try {
      if (purchaseUpdateSubscription) {
        purchaseUpdateSubscription.remove();
        purchaseUpdateSubscription = null;
      }
      if (purchaseErrorSubscription) {
        purchaseErrorSubscription.remove();
        purchaseErrorSubscription = null;
      }
      RNIap.endConnection();
    } catch (e) {}
  };
  useEffect(() => {
    if (!user) {
      disconnect();
    } else {
      setTimeout(() => {
        if (!purchaseUpdateSubscription) {
          // console.log('SUBS');
          init();
        }
      }, 250);
    }
  }, [user]);

  useEffect(() => {
    return () => {
      disconnect();
    };
  }, []);
  const handlePay = async ({sku}) => {
    renewal = false;
    setSkuSelected(sku);
    try {
      await RNIap.requestSubscription(sku);
    } catch (err) {}
  };
  const handleFeedback = () => {
    if (skuSelected) {
      return;
    }
    dispatch(setModalSuscriptionAction(false));
    setShowModalSuccess(false);
  };

  const handleCloseModal = () => {
    dispatch(setModalSuscriptionAction(false));
    setShowModalSuccess(false);
  };
  return (
    <>
      <Modal visible={showModalSuscription} transparent animationType="fade">
        <TouchableWithoutFeedback onPress={handleFeedback}>
          <View style={[styles.maskModal]} pointerEvents={skuSelected ? 'none' : 'auto'}>
            <View style={styles.contentModal}>
              {showModalSuccess && user?.premium_valid_until ? (
                <View style={Styles.marginBottom18}>
                  <Text align="center" fontSize={16} color="#0B2E40">
                    Felicidades, ya tienes plan Premium Válido hasta {user?.premium_valid_until}
                  </Text>
                </View>
              ) : (
                <>
                  <Text align="left" fontSize={24} weight="Heavy" color="#CAB285">
                    Obtener plan Premium
                  </Text>
                  <View style={styles.contentBenefits}>
                    {benefitsList.map((benefits, index) => {
                      return (
                        <View style={[Styles.row, Styles.marginBottom21]} key={index}>
                          <View style={Styles.marginRight10}>
                            <Icon name="check" color={Colors.blue} size={19} />
                          </View>
                          <Text align="left" fontSize={16} color="#7D7B7B">
                            {benefits}
                          </Text>
                        </View>
                      );
                    })}
                  </View>
                  <View style={[Styles.center]}>
                    {buttonPrices.map((btnPrices, index) => {
                      return (
                        <TouchableOpacity
                          key={index}
                          style={styles.buttonPrice}
                          onPress={() => handlePay(btnPrices)}>
                          {skuSelected === btnPrices.sku ? (
                            <ActivityIndicator color={Colors.white} />
                          ) : (
                            <Text weight="Black" fontSize={14} color={Colors.white}>
                              {btnPrices.title}
                            </Text>
                          )}
                        </TouchableOpacity>
                      );
                    })}
                  </View>
                </>
              )}
              <View style={Styles.center}>
                <TouchableOpacity style={styles.buttonClose} onPress={handleCloseModal}>
                  <Text weight="Black" fontSize={14} color={Colors.darkLight}>
                    Cerrar
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </TouchableWithoutFeedback>

        {children}
      </Modal>
    </>
  );
};

const mapStateToProps = ({user, dispatch, showModalSuscription, deviceToken}) => ({
  user,
  dispatch,
  showModalSuscription,
  deviceToken,
});

export default connect(mapStateToProps)(ButtonPremium);
