import React from 'react';
import {connect} from 'react-redux';
import {
  View,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Modal,
  StyleSheet,
  Keyboard,
} from 'react-native';
import Text from '@components/Text';
import {setModalAction} from '@actions/modal.actions';
import {styles as Styles} from '@assets/styles';
import responsiveSize from 'react-native-normalize';
import Colors from '@assets/Colors';
import FastImage from 'react-native-fast-image';
import {Modal as ModalInterface} from '../utils/interface/interfaceHelper';

const styles = StyleSheet.create({
  contentButtonSubscription: {
    justifyContent: 'flex-end',
    paddingBottom: '5%',
  },
  center: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonPremium: {
    backgroundColor: Colors.danger,
    width: responsiveSize(176),
    height: responsiveSize(44),
    borderRadius: 22,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 10,
  },
  contentModal: {
    marginHorizontal: responsiveSize(25),
    backgroundColor: Colors.lightBlue,
    borderRadius: 20,
    paddingTop: responsiveSize(35),
    paddingHorizontal: responsiveSize(25),
    paddingVertical: 10,
    shadowColor: Colors.black,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  maskModal: {flex: 1, backgroundColor: Colors.mask, justifyContent: 'center'},
  contentBenefits: {paddingRight: '13%', paddingVertical: '10%', paddingTop: '10%'},
  buttonClose: {
    backgroundColor: Colors.white,
    width: responsiveSize(139),
    height: responsiveSize(45),
    borderRadius: 22,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 10,
  },
  buttonPrice: {
    borderRadius: 22,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 10,
    height: responsiveSize(45),
    width: responsiveSize(205),
    backgroundColor: Colors.blue,
    ...Styles.center,
  },
});

const GlobalModal = ({modal, dispatch}) => {
  const handleFeedback = () => {
    Keyboard.dismiss();
    dispatch(setModalAction({isVisible: false}));
  };

  const handleConfirmModal = () => {
    if (modal?.onConfirm) {
      modal?.onConfirm();
    }
    dispatch(setModalAction({isVisible: false}));
  };
  const handleCloseModal = () => {
    Keyboard.dismiss();
    if (modal?.onClose) {
      modal?.onClose();
    }
    if (modal?.onCancel) {
      modal?.onCancel();
    }
    dispatch(setModalAction({isVisible: false}));
  };
  return (
    <Modal visible={modal?.isVisible} transparent animationType="fade">
      <TouchableWithoutFeedback onPress={handleFeedback}>
        <View style={[styles.maskModal]}>
          <View style={styles.contentModal}>
            {modal?.title && (
              <View style={Styles.marginBottom18}>
                <Text weight="Heavy" fontSize={20}>
                  {modal?.title}
                </Text>
              </View>
            )}
            {modal?.image && (
              <View style={[Styles.marginBottom30, Styles.center]}>
                <FastImage
                  resizeMode="cover"
                  source={modal?.image}
                  style={{
                    width: responsiveSize(modal?.widthImage || 155),
                    height: responsiveSize(modal?.heightImage || 152),
                  }}
                />
              </View>
            )}
            {modal?.description && (
              <View style={Styles.marginBottom30}>
                <Text fontSize={16} align={modal?.isActionSheet ? 'left' : modal?.alingDescription}>
                  {modal?.description}
                </Text>
              </View>
            )}
            {modal?.isActionSheet && (
              <View style={[Styles.rowLarge]}>
                <TouchableOpacity
                  style={[styles.buttonClose, {flex: 1, marginRight: 5}]}
                  onPress={handleConfirmModal}>
                  <Text weight="Black" fontSize={14} color={Colors.darkLight}>
                    Si
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={[styles.buttonClose, {flex: 1, marginLeft: 5}]}
                  onPress={handleCloseModal}>
                  <Text weight="Black" fontSize={14} color={Colors.darkLight}>
                    No
                  </Text>
                </TouchableOpacity>
              </View>
            )}
            {!modal?.isActionSheet && (
              <View style={Styles.center}>
                <TouchableOpacity style={styles.buttonClose} onPress={handleCloseModal}>
                  <Text weight="Black" fontSize={14} color={Colors.darkLight}>
                    Cerrar
                  </Text>
                </TouchableOpacity>
              </View>
            )}
          </View>
        </View>
      </TouchableWithoutFeedback>
    </Modal>
  );
};

const mapStateToProps = ({user, dispatch, modal}) => ({
  user,
  dispatch,
  modal,
});

export default connect(mapStateToProps)(GlobalModal);
