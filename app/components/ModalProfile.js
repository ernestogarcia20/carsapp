import React, {useState, useEffect} from 'react';
import {connect} from 'react-redux';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Platform,
  ActivityIndicator,
} from 'react-native';
import {
  signInAppleAction,
  signInFacebookAction,
  signInGoogleAction,
  logOutAction,
  updateUserStoreAction,
} from '@actions/auth.actions';
import LocalIcon from '@components/Icon';
import Icon from 'react-native-vector-icons/FontAwesome5';
import ButtonPremium from '@components/ButtonPremium';
import ResponseSize from 'react-native-normalize';
import BottomSheet from 'reanimated-bottom-sheet';
import Text from '@components/Text';
import Animated from 'react-native-reanimated';
import Colors from '@assets/Colors';
import {homeAction} from '@actions/home.actions';
import {FontRoman} from '@constants/fonts';

const HEIGTH = ResponseSize(Platform.OS === 'android' ? 410 : 450);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.black,
  },
  contentSwiper: {
    height: HEIGTH,
    backgroundColor: Colors.lightBlue,
    paddingTop: '5%',
  },
  swiper: {
    backgroundColor: Colors.lightBlue,
    paddingTop: '5%',
  },
  buttonPremium: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: Platform.OS === 'android' ? 33 : 25,
  },
  contentAnimated: {flex: 1, backgroundColor: Colors.lightBlue},
  header: {
    backgroundColor: Colors.lightBlue,
    shadowColor: Colors.black,
    paddingTop: 15,
    borderTopLeftRadius: 33,
    borderTopRightRadius: 33,
  },
  center: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  loader: {
    flex: 0.9,
  },
  panelHeader: {
    alignItems: 'center',
  },
  panelHandle: {
    width: 73,
    height: 5,
    borderRadius: 4,
    backgroundColor: '#00000040',
    marginBottom: 10,
  },
  buttonSocial: {
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    width: 55,
    height: 55,
    borderWidth: 1,
    borderColor: '#C3C3C3',
    marginHorizontal: 15,
  },
  marginHorizontal: {
    marginHorizontal: '10%',
  },
  contentButtons: {
    marginTop: 39,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  contentCard: {
    borderRadius: 13,
    backgroundColor: Colors.white,
    shadowColor: Colors.black,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    zIndex: 2,
    paddingHorizontal: '6%',
    paddingVertical: '5%',
    paddingTop: '7%',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  rowLarge: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  separator: {
    paddingBottom: 24,
  },
  logOut: {
    marginVertical: 3,
  },
  swipe: {paddingHorizontal: '5%', flex: 1},
  profile: {paddingHorizontal: '5%'},
  contentEmailSupport: {
    justifyContent: 'flex-end',
    flex: Platform.OS === 'android' ? 0.09 : 0.06,
    marginBottom: 6,
  },
  iconApple: {marginBottom: 5},
  descriptionLogin: {flex: 0.8, justifyContent: 'center'},
  contentProfileData: {marginTop: 30, flex: 0.8},
  absoluteButton: {
    position: 'absolute',
    left: 0,
    bottom: 0,
    right: 0,
    top: 0,
    zIndex: 1,
  },
});

const fall = new Animated.Value(1);
let oldUser = {};
// sheetRef.current.snapTo(0)
const ModalProfile = ({
  children,
  showModal,
  setState,
  dispatch,
  user,
  deviceToken,
  onPressPlans = () => {},
}) => {
  const sheetRef = React.useRef(null);
  const [loader, setLoader] = useState(false);

  useEffect(() => {
    if (showModal) {
      setTimeout(() => {
        sheetRef.current.snapTo(0);
      }, 5);
    }
  }, [showModal]);

  const getHomeLoad = async () => {
    await dispatch(homeAction(deviceToken));
  };

  useEffect(() => {
    if (!user) {
      oldUser = {};
      return;
    }
    if (user?.id === oldUser?.id) {
      return;
    }
    oldUser = user;
    // console.log('HOME LOAD');
    getHomeLoad();
  }, [user]);

  const onCloseModal = () => {
    sheetRef.current.forceUpdate();
    setState(false);
  };

  const handleSignInGoogle = async () => {
    setLoader(true);
    try {
      await dispatch(signInGoogleAction(deviceToken));
      setLoader(false);
    } catch (e) {
      setLoader(false);
    }
  };
  const handleSignInApple = async () => {
    setLoader(true);
    try {
      await dispatch(signInAppleAction(deviceToken));
      setLoader(false);
    } catch (e) {
      setLoader(false);
    }
  };
  const handleSignInFacebook = async () => {
    setLoader(true);
    try {
      await dispatch(signInFacebookAction(deviceToken));
      setLoader(false);
    } catch (e) {
      setLoader(false);
    }
  };

  const handleFeedback = () => {
    try {
      sheetRef.current.forceUpdate();
      sheetRef.current.snapTo(1);
    } catch (e) {}
  };

  const onPressLogOut = async () => {
    await dispatch(logOutAction());
  };

  const SignInComponent = () => {
    return (
      <View style={styles.swipe}>
        <View style={styles.profile}>
          <Text color={Colors.darkBlue} align="left" weight="Heavy" fontSize={20}>
            Perfil de usuario
          </Text>
        </View>
        <View style={styles.descriptionLogin}>
          <Text color="#4F4F4F" fontSize={16} weight={FontRoman}>
            Para obtener plan Premium{'\n'} primero debes ingresar con:
          </Text>

          <View style={styles.contentButtons}>
            <TouchableOpacity style={styles.buttonSocial} onPress={handleSignInFacebook}>
              <LocalIcon name="facebook" width={25} heigth={25} />
            </TouchableOpacity>
            <TouchableOpacity style={[styles.buttonSocial]} onPress={handleSignInGoogle}>
              <LocalIcon name="google" width={23} heigth={25} />
            </TouchableOpacity>
            {Platform.OS === 'ios' && (
              <TouchableOpacity style={styles.buttonSocial} onPress={handleSignInApple}>
                <Icon name="apple" style={styles.iconApple} size={28} />
              </TouchableOpacity>
            )}
          </View>
        </View>
      </View>
    );
  };

  const ProfileComponent = () => {
    return (
      <View style={styles.swipe}>
        <View style={styles.profile}>
          <Text color={Colors.darkBlue} align="left" weight="Black" fontSize={20}>
            Perfil de usuario
          </Text>
        </View>
        <View style={styles.contentProfileData}>
          <View style={styles.contentCard}>
            <View style={[styles.rowLarge, styles.separator]}>
              <Text color={Colors.darkLight} fontSize={16} weight="Heavy">
                Nombre
              </Text>
              <Text color={Colors.darkBlue} fontSize={17} weight="Heavy">
                {user?.name || '-------'}
              </Text>
            </View>
            <View style={[styles.rowLarge, styles.separator]}>
              <Text color={Colors.darkLight} fontSize={16} weight="Heavy">
                Plan
              </Text>
              <Text
                color={user?.is_premium ? Colors.green : Colors.danger}
                fontSize={17}
                weight="Heavy">
                {user?.is_premium ? 'Premium' : 'Básico'}
              </Text>
            </View>
            {user?.is_premium && (
              <View style={[styles.rowLarge, styles.separator]}>
                <Text color={Colors.darkLight} fontSize={16} weight="Heavy">
                  Plan válido hasta
                </Text>
                <Text color={Colors.darkBlue} fontSize={17} weight="Heavy">
                  {user?.premium_valid_until || ''}
                </Text>
              </View>
            )}
            <View style={[styles.rowLarge, styles.separator]}>
              <Text color={Colors.darkLight} fontSize={16} weight="Heavy">
                ID
              </Text>
              <Text color={Colors.darkBlue} fontSize={17} weight="Heavy">
                {user?.public_id || 0}
              </Text>
            </View>
            <TouchableOpacity style={styles.logOut} onPress={onPressLogOut}>
              <Text fontSize={17} color={Colors.blue} weight="Heavy">
                Cerrar sesión
              </Text>
            </TouchableOpacity>
          </View>
          <View style={[styles.center, styles.buttonPremium]}>
            <ButtonPremium showTextPremium={false} />
          </View>
        </View>
        <View style={styles.contentEmailSupport}>
          <Text color={Colors.darkLight} align="center">
            autodato@centauribrain.com
          </Text>
        </View>
      </View>
    );
  };
  const renderContent = () => (
    <View style={styles.contentSwiper}>
      {loader && (
        <View style={[styles.center, styles.loader]}>
          <ActivityIndicator size="large" color={Colors.black} />
        </View>
      )}
      {!loader && !user && SignInComponent()}
      {!loader && user && ProfileComponent()}
    </View>
  );
  const renderHeader = () => (
    <View style={styles.header}>
      <View style={styles.panelHeader}>
        <View style={styles.panelHandle} />
      </View>
    </View>
  );
  return (
    <>
      <View style={styles.container} pointerEvents={loader ? 'none' : 'auto'}>
        <Animated.View
          style={[
            styles.contentAnimated,
            {opacity: Animated.add(0.5, Animated.multiply(fall, 1))},
          ]}>
          {showModal && (
            <TouchableWithoutFeedback onPress={handleFeedback} style={styles.absoluteButton}>
              <View style={styles.absoluteButton} />
            </TouchableWithoutFeedback>
          )}
          <View pointerEvents={showModal ? 'none' : 'auto'} style={{flex: 1}}>
            {children}
          </View>
        </Animated.View>

        {showModal && (
          <BottomSheet
            ref={sheetRef}
            snapPoints={[HEIGTH, -50, -50]}
            initialSnap={1}
            enabledInnerScrolling={false}
            renderHeader={renderHeader}
            onCloseEnd={onCloseModal}
            callbackNode={fall}
            renderContent={renderContent}
          />
        )}
      </View>
    </>
  );
};

const mapStateToProps = ({user, dispatch, deviceToken}) => ({
  user,
  deviceToken,
  dispatch,
});

export default connect(mapStateToProps)(ModalProfile);
