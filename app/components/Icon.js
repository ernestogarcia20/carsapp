import React from 'react';
import {View, Platform} from 'react-native';
import PropTypes from 'prop-types';
import SvgUri from 'react-native-svg-uri';
import Colors from '@assets/Colors';
import {SvgCssUri} from 'react-native-svg';
import resolveAssetSource from 'react-native/Libraries/Image/resolveAssetSource';
import FastImage from 'react-native-fast-image';
import ProfileSvg from '../icons/profile.svg';
import FacebookSvg from '../icons/facebook.svg';
import GoogleSvg from '../icons/google.svg';
import AppleSvg from '../icons/apple.svg';
import ProfilePng from '../icons/profile.png';
import FacebookPng from '../icons/facebook.png';
import GooglePng from '../icons/google.png';
import ApplePng from '../icons/apple.png';

const svg = {
  profile: Platform.OS === 'ios' ? ProfileSvg : ProfilePng,
  facebook: Platform.OS === 'ios' ? FacebookSvg : FacebookPng,
  google: Platform.OS === 'ios' ? GoogleSvg : GooglePng,
  apple: Platform.OS === 'ios' ? AppleSvg : ApplePng,
};
const Icon = (props) => {
  const {name, width, heigth: height, fill} = props;
  const IconSvg = svg[name] ? svg[name] : null;
  if (!IconSvg) {
    return <View />;
  }
  return Platform.OS === 'ios' ? (
    <SvgUri width={String(width)} height={String(height)} source={IconSvg} fill={fill} />
  ) : (
    <FastImage source={IconSvg} resizeMode="contain" style={{width, height}} />
  );
};

Icon.propTypes = {
  width: PropTypes.number,
  heigth: PropTypes.number,
  name: PropTypes.string.isRequired,
  fill: PropTypes.string,
};

Icon.defaultProps = {
  width: 40,
  heigth: 40,
  fill: undefined,
};
export default Icon;
