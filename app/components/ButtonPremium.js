import React from 'react';
import {TouchableOpacity, View, StyleSheet} from 'react-native';
import {setModalSuscriptionAction} from '@actions/subscriptions.actions';
import {connect} from 'react-redux';
import Text from '@components/Text';
import Colors from '@assets/Colors';
import {styles as Styles} from '@assets/styles';
import responsiveSize from 'react-native-normalize';

const styles = StyleSheet.create({
  contentButtonSubscription: {
    justifyContent: 'flex-end',
    paddingBottom: '5%',
  },
  buttonPremium: {
    backgroundColor: Colors.danger,
    width: responsiveSize(176),
    height: responsiveSize(44),
    borderRadius: 22,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 10,
  },
});

const ButtonPremium = ({user, showTextPremium = true, onPressProfile = () => {}, dispatch, style = {}}) => {
  const handleShowModal = () => {
    if (!user) {
      onPressProfile();
      return;
    }
    dispatch(setModalSuscriptionAction(true));
  };
  return (
    <>
      <View style={[styles.contentButtonSubscription, {...style}]}>
        {!user?.is_premium && (
          <View style={Styles.center}>
            <TouchableOpacity style={styles.buttonPremium} onPress={handleShowModal}>
              <Text weight="Black" color={Colors.white} fontSize={15}>
                Obtener Premium
              </Text>
            </TouchableOpacity>
          </View>
        )}
        {showTextPremium && (
          <Text
            fontSize={16}
            weight="Medium"
            color={!user?.is_premium ? Colors.danger : Colors.green}>
            {user?.is_premium ? 'Plan Premium' : 'Plan básico'}
          </Text>
        )}
      </View>
    </>
  );
};

const mapStateToProps = ({user, dispatch}) => ({
  user,
  dispatch,
});

export default connect(mapStateToProps)(ButtonPremium);
