import React, {Fragment} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  StatusBar,
  View,
  ViewPropTypes,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
import Colors from '@assets/Colors';
import PropTypes from 'prop-types';
import FastImage from 'react-native-fast-image';
import Images from '@assets/Images';

const styles = StyleSheet.create({
  background: {top: 0, left: 0, right: 0, bottom: 0, position: 'absolute'},
  container: {flex: 1, backgroundColor: Colors.lightBlue, paddingHorizontal: 10},
  contentKeyboard: {flex: 0.9},
  loader: {
    flex: 1,
    backgroundColor: Colors.white,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bottomSafeArea: {flex: 0, backgroundColor: Colors.lightBlue},
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  modalView: {
    margin: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  activityIndicator: {
    marginRight: 15,
  },
  contentBackground: {flex: 1, backgroundColor: 'black'},
});

/* export const loader = (propLoading) => {
  const { isLoading, text } = propLoading;
  return (
    <Modal animationType="fade" transparent visible={isLoading}>
      <View style={styles.centeredView}>
        <FastImage source={LoadingGif} style={{ width: 110, height: 110 }} />
        <View style={styles.modalView}>
          <ActivityIndicator size="small" color={Colors.white} style={styles.activityIndicator} />
          <Text style={styles.modalText} color={Colors.white} fontSize={18}>
            {I18n.t(`loading.${text}`)}
          </Text>
        </View>
      </View>
    </Modal>
  );
}; */

const Container = ({children, style, showStatusBar, isOnBoard, padding, showBackground}) => {
  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
      <>
        <FastImage
          style={styles.contentBackground}
          source={showBackground ? Images.background : null}>
          <StatusBar
            backgroundColor={Colors.lightBlue}
            hidden={!showStatusBar}
            barStyle="dark-content"
          />
          {isOnBoard ? (
            children
          ) : (
            <>
              <SafeAreaView style={styles.bottomSafeArea} />

              <SafeAreaView
                onStartShouldSetResponder={Keyboard.dismiss}
                style={[styles.container, {...style, paddingHorizontal: padding ? 10 : 0}]}>
                {/* loader(propLoading) */}

                {showBackground ? (
                  <FastImage style={{flex: 1}} source={Images.background}>
                    {children}
                  </FastImage>
                ) : (
                  <View style={{flex: 1}}>{children}</View>
                )}
              </SafeAreaView>
            </>
          )}
        </FastImage>
      </>
    </TouchableWithoutFeedback>
  );
};

Container.propTypes = {
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]).isRequired,
  style: ViewPropTypes.style,
  showStatusBar: PropTypes.bool,
  isOnBoard: PropTypes.bool,
  padding: PropTypes.bool,
  showBackground: PropTypes.bool,
};

Container.defaultProps = {
  style: ViewPropTypes.style,
  showStatusBar: true,
  padding: false,
  isOnBoard: false,
  showBackground: false,
};

export default Container;
