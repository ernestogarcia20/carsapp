import React from 'react';
import {StyleSheet, TouchableOpacity, ViewPropTypes, View} from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/Feather';
import Colors from '@assets/Colors';
import ReponseSize from 'react-native-normalize';
import {styles as Styles} from '@assets/styles';
import FastImage from 'react-native-fast-image';
import Images from '@assets/Images';
import LocalIcon from './Icon';
import Text from './Text';

const styles = StyleSheet.create({
  header: {
    backgroundColor: Colors.lightBlue,
    height: 60,
    zIndex: 2,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  background: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  arrowBack: {justifyContent: 'center', alignItems: 'center', marginLeft: 20},
  iconBell: {justifyContent: 'center', alignItems: 'center', marginRight: 20},
  absoluteTitle: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
    left: 0,
    right: 0,
    zIndex: -1,
  },
  absoluteTitleLeft: {
    position: 'absolute',
    justifyContent: 'center',
    left: 15,
    zIndex: -1,
  },
  titleWrap: {flexWrap: 'wrap'},
  point: {
    width: 10,
    height: 10,
    borderRadius: 100,
    backgroundColor: '#FF0000',
    position: 'absolute',
    right: 0,
    top: 0,
    zIndex: 1,
  },
  paddingIcon: {paddingHorizontal: 10},
  contentActions: {flexDirection: 'row', alignItems: 'center', paddingRight: '3%'},
  contentTitle: {maxWidth: '65%', flexDirection: 'row', flexWrap: 'wrap'},
});

const Header = ({
  title,
  color,
  style,
  isProfile,
  onBack,
  onPressProfile,
  onPressInfo,
  height = 60,
  info,
}) => {
  return (
    <View style={[style, styles.header, {height}]}>
      {!isProfile ? (
        <TouchableOpacity onPress={onBack} style={styles.arrowBack}>
          <Icon name="chevron-left" size={30} color={Colors.black} />
        </TouchableOpacity>
      ) : (
        <View />
      )}
      <View style={[styles.absoluteTitle]}>
        <View style={styles.contentTitle}>
          <Text color={color} style={styles.titleWrap} fontSize={ReponseSize(22)} weight="Heavy">
            {title}
          </Text>
        </View>
      </View>
      <View style={styles.contentActions}>
        {isProfile && (
          <TouchableOpacity style={styles.paddingIcon} onPress={onPressProfile}>
            <LocalIcon width={34} height={34} name="profile" />
          </TouchableOpacity>
        )}
        {info && (
          <TouchableOpacity style={[{marginRight: 15}, Styles.center]} onPress={onPressInfo}>
            <FastImage source={Images.info} resizeMode="cover" style={{width: 28, height: 28}} />
          </TouchableOpacity>
        )}
      </View>
    </View>
  );
};

Header.propTypes = {
  title: PropTypes.string,
  style: ViewPropTypes.style,
  color: PropTypes.string,
  onBack: PropTypes.func,
  onPressProfile: PropTypes.func,
  onPressInfo: PropTypes.func,
  isProfile: PropTypes.bool,
  info: PropTypes.bool,
  height: PropTypes.number,
};

Header.defaultProps = {
  title: '',
  style: ViewPropTypes.style,
  color: Colors.black,
  onBack: () => {},
  onPressProfile: () => {},
  onPressInfo: () => {},
  info: false,
  isProfile: false,
  height: 50,
};

export default Header;
