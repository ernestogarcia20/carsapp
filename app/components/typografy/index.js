import {Platform} from 'react-native';
import { FontRegular, FontRoman } from '@constants/fonts';

// we define available font weight and styles for each font here
const font = {
  Montserrat: {
    weights: {
      ExtraBold: '900',
      Bold: '700',
      SemiBold: '600',
      Medium: '500',
      Regular: '400',
      Light: '300',
      Thin: '100',
    },
    styles: {
      Italic: 'italic',
    },
  },
};

// generate styles for a font with given weight and style
export const fontMaker = (
  weight = FontRoman,
  fontStyle = 'normal',
  family = 'Avenir',
) => {
  // const {weights} = font[family];
  return {
    fontFamily: `${family}-${weight}`,
  };
};
