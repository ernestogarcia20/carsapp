import React from 'react';
import {TouchableOpacity, StyleSheet, ActivityIndicator} from 'react-native';
import Text from '@components/Text';
import ResponsiveSize from 'react-native-normalize';
import Colors from '@assets/Colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

const Layout = ({
  text = '',
  width = 220,
  height = 44,
  buttonColor = Colors.blue,
  textColor = Colors.white,
  fontSize = 16,
  onPress = () => {},
  loading = false,
}) => {
  return (
    <TouchableOpacity
      style={{
        height: ResponsiveSize(height),
        width: ResponsiveSize(width),
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: buttonColor,
        borderRadius: 22,
      }}
      onPress={loading ? null : onPress}>
      {loading ? (
        <ActivityIndicator color={textColor} />
      ) : (
        <Text color={textColor} fontSize={fontSize}>
          {text}
        </Text>
      )}
    </TouchableOpacity>
  );
};

export default Layout;
