import React from 'react';
import {View, TextInput, StyleSheet} from 'react-native';
import Colors from '@assets/Colors';
import Text from '@components/Text';
import ResponsiveSize from 'react-native-normalize';

const styles = StyleSheet.create({
  container: {},
  input: {
    height: ResponsiveSize(49),
    borderRadius: 22,
    backgroundColor: Colors.white,
    justifyContent: 'center',
    shadowColor: 'rgba(0,0,0,0.3)',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    paddingHorizontal: '5%',
  },
  contentText: {marginLeft: '5%', marginBottom: 7},
});

const Layout = ({
  text,
  value,
  textColor = Colors.black,
  attrName,
  updateMasterState = () => {},
}) => {
  return (
    <View style={styles.container}>
      <View style={styles.contentText}>
        <Text align="left" fontSize={16} color={textColor}>
          {text}
        </Text>
      </View>
      <TextInput
        style={styles.input}
        value={value}
        maxLength={6}
        onChangeText={(changeText) => updateMasterState(attrName, changeText)}
      />
    </View>
  );
};

export default Layout;
