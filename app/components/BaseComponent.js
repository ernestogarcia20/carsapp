import React, {Component} from 'react';

import {setModalAction} from '@actions/modal.actions';
import {ModalActionSheet} from '../utils/interface/interfaceHelper';

export default class BaseReact extends Component {
  goBack = () => {
    const {
      navigation: {goBack},
    } = this.props;
    goBack();
  };

  setModalGlobal = (modalProps: ModalActionSheet, dispatch) => {
    if (!modalProps.title) {
      modalProps.title = null;
    }
    if (!modalProps.alingDescription) {
      modalProps.alingDescription = 'center';
    }
    if (!modalProps.description) {
      modalProps.description = null;
    }
    if (!modalProps.onClose) {
      modalProps.onClose = () => {};
    }
    dispatch(setModalAction(modalProps));
  };

  openDrawer = () => {
    const {
      navigation: {openDrawer},
    } = this.props;

    openDrawer();
  };

  handleCloseDrawer = () => {
    const {
      navigation: {closeDrawer},
    } = this.props;
    closeDrawer();
  };

  navigate = (name, params) => {
    const {
      navigation: {navigate},
    } = this.props;
    navigate(name, params);
  };
}
