import React from 'react';
import {createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import FastImage from 'react-native-fast-image';
import Images from '@assets/Images';
import responsiveSize from 'react-native-normalize';
import Colors from '@assets/Colors';

import OnBoard from '@screens/onboard/onboard.controller';
import Splash from '@screens/splash/splash.controller';
import Home from '@screens/home/home.controller';
import PatentReport from '@screens/patent-report/patent-report-controller';
import PatentReportPremium from '@screens/patent-report-premium/patent-report-premium-controller';
import ReportDetail from '@screens/report-detail/report-detail-controller';
import MyReports from '@screens/my-reports/my-reports-controller';
import PatentSectionDetail from '@screens/patent-section-detail/patent-section-detail-controller';

const tabbarVisible = (navigation) => {
  const {routes} = navigation.state;
  
  // console.log(navigation.getParam('tabBarVisible', null));
  let showTabbar = true;
  routes?.forEach((route) => {
    if (route.routeName === 'ReportDetail') {
      showTabbar = false;
    }
  });

  return showTabbar;
};

const PatentPremiumStack = createStackNavigator(
  {
    PatentReportPremium,
    ReportDetail,
    PatentSectionDetail,
  },
  {
    initialRouteName: 'PatentReportPremium',
    headerMode: 'none',
  }
);

const PatentStack = createStackNavigator(
  {
    PatentReport,
    ReportDetail,
  },
  {
    initialRouteName: 'PatentReport',
    headerMode: 'none',
  }
);

const MyReportsStack = createStackNavigator(
  {
    MyReports,
    ReportDetail,
    PatentSectionDetail,
  },
  {
    initialRouteName: 'MyReports',
    headerMode: 'none',
  }
);

const PatentPremiumNav = createBottomTabNavigator(
  {
    PatentPremiumStack: {
      screen: PatentPremiumStack,
      navigationOptions: {
        tabBarLabel: 'BUSCAR',
      },
    },
    MyReports: {
      screen: MyReportsStack,
      navigationOptions: {
        tabBarLabel: 'MIS INFORMES',
      },
    },
  },
  {
    initialRouteName: 'PatentPremiumStack',
    tabBarOptions: {
      activeTintColor: Colors.blue,
      inactiveTintColor: '#C3C3C3',
      allowFontScaling: false,
      labelStyle: {
        fontFamily: 'Avenir',
        fontSize: responsiveSize(12),
        marginBottom: 1,
      },
    },
    defaultNavigationOptions: ({navigation}) => ({
      tabBarVisible: tabbarVisible(navigation),
      tabBarIcon: ({focused, horizontal, tintColor}) => {
        const {routeName} = navigation.state;
        let iconName;
        if (routeName === 'PatentReport') {
          iconName = focused ? Images.searchActive : Images.searchInactive;
          // Sometimes we want to add badges to some icons.
          // You can check the implementation below.
        } else {
          iconName = focused ? Images.patent : Images.myReportsInactive;
        }

        // You can return any component that you like here!
        return (
          <FastImage
            source={iconName}
            resizeMode="cover"
            style={{height: responsiveSize(17), width: responsiveSize(17), marginTop: 5}}
          />
        );
      },
    }),
  }
);

const HomeStack = createStackNavigator(
  {
    Home,
    ReportDetail,
    PatentStack,
    PatentPremiumNav,
  },
  {
    initialRouteName: 'Home',
    headerMode: 'none',
  }
);

export default createSwitchNavigator(
  {
    OnBoard,
    Splash,
    Home: HomeStack,
  },
  {
    initialRouteName: 'Splash',
  }
);
