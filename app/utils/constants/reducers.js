/**
 * DISPATCH
 */

export const SET_DISPATCH = 'SET_DISPATCH';

/**
 * USER
 */

export const SET_USER = 'SET_USER';
export const UPDATE_USER = 'UPDATE_USER';
export const REMOVE_USER = 'REMOVE_USER';

/**
 * DEVICE TOKEN
 */
export const SET_DEVICE_TOKEN = 'SET_DEVICE_TOKEN';

/**
 * INTERNET
 */
export const SET_INTERNET = 'SET_INTERNET';

/**
 * ONBOARD
 */
export const SET_ONBOARD = 'SET_ONBOARD';

/**
 * PATENT REPORT
 */
export const SET_PATENT_REPORT = 'SET_PATENT_REPORT';

/**
 * LOADING
 */
export const SET_LOADING = 'SET_LOADING';

/**
 * LOADING
 */
export const SET_MODAL = 'SET_MODAL';

/**
 * MODAL SUSCRIPTION
 */
export const SET_MODAL_SUSCRIPTION = 'SET_MODAL_SUSCRIPTION';
export const SET_MODAL_SUSCRIPTION_STARTED = 'SET_MODAL_SUSCRIPTION_STARTED';

export const REMOVE_ALL = 'REMOVE_ALL';
