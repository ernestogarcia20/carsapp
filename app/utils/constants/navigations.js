export const NAVIGATE_ONBOARD = 'OnBoard';
export const NAVIGATE_SPLASH = 'Splash';
export const NAVIGATE_HOME = 'Home';
export const NAVIGATE_PATENT_REPORT = 'PatentReport';
export const NAVIGATE_REPORT_DETAIL = 'ReportDetail';
export const NAVIGATE_PATENT_PREMIUM_NAV = 'PatentPremiumNav';
export const NAVIGATE_PATENT_SECTION_DETAIL = 'PatentSectionDetail';
