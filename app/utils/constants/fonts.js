/**
 * FONST
 */
export const FontBlack = 'Black';
export const FontBold = 'Bold';
export const FontLight = 'Light';
export const FontRegular = 'Regular';
export const FontItalic = 'Italic';
export const FontMedium = 'Medium';
export const FontThin = 'Thin';
export const FontHeavy = 'Heavy';
export const FontRoman = 'Roman';
