export const getInitials = string => {
  const names = string.split(' ');
  let initials = names[0].substring(0, 1).toUpperCase();

  if (names.length > 1) {
    initials += names[names.length - 1].substring(0, 1).toUpperCase();
  }
  return initials;
};

export const isEmptyObj = (obj) => {
  return JSON.stringify(obj) === JSON.stringify({});
};

export const validateEmail = email => {
  const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
};

export const setStore = (ACCION, data) => {
  return {type: ACCION, payload: data};
};

export const stringToDate = (_date, _format, _delimiter) => {
  var formatLowerCase = _format.toLowerCase();
  var formatItems = formatLowerCase.split(_delimiter);
  var dateItems = _date.split(_delimiter);
  var monthIndex = formatItems.indexOf('mm');
  var dayIndex = formatItems.indexOf('dd');
  var yearIndex = formatItems.indexOf('yyyy');
  var year = parseInt(dateItems[yearIndex]);
  // adjust for 2 digit year
  if (year < 100) {
    year += 2000;
  }
  var month = parseInt(dateItems[monthIndex]);
  month -= 1;
  var formatedDate = new Date(year, month, dateItems[dayIndex]);
  return formatedDate;
};

export const defaultDate = '01/01/1990';
