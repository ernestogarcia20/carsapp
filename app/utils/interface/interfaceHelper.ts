
export interface Modal {
    isVisible: boolean,
    image: string,
    widthImage: number,
    heightImage: number,
    alingDescription: 'auto' | 'left' | 'right' | 'center' | 'justify',
    title: string,
    description: string,
    onClose: Function | null,
}

export interface ModalActionSheet extends  Modal {
    isActionSheet: boolean | false,
    onConfirm: Function | null,
    onCancel: Function | null,
}