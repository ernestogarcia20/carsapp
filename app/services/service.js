import apisauce from 'apisauce';
/* import auth from '@react-native-firebase/auth';
import perf from '@react-native-firebase/perf'; */
import auth from '@react-native-firebase/auth';
import {Platform} from 'react-native';
import config from '../config';

const create = () => {
  const api = apisauce.create({
    baseURL: config.urlApi,
    timeout: 50000,
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
  });

  const GetOnBoard = async () => {
    const response = await api.get('OnBoard/Get');
    return response;
  };
  const GetUser = async (deviceToken = null) => {
    const response = await api.get(
      `user/get?user_id=${auth().currentUser.uid}&device_token=${deviceToken}`
    );
    return response;
  };
  const CreateUser = async (user) => {
    const data = {
      user_data: {
        id: auth().currentUser.uid || '',
        firstname: user?.firstname || '',
        middlename: user?.middlename || '',
        lastname: user?.lastname || '',
        fullname: user?.fullname || '',
        email: user?.email || '',
        gender: user?.gender || '',
        birthdate: user?.birthdate || '',
        device_token: user?.device_token || '',
      },
    };
    const response = await api.post('user/create', data);
    return response;
  };
  const GetHome = async (deviceToken) => {
    const response = await api.get(
      `Home/Load?device_token=${deviceToken}&user_id=${auth().currentUser.uid}`
    );
    return response;
  };
  const CheckSubscription = async (originalTransactionId) => {
    const response = await api.get(
      `User/CheckSubscription?original_transaction_id=${originalTransactionId}`
    );
    return response;
  };
  const MoveSubscription = async (data) => {
    const response = await api.post('User/MoveSubscription', data);
    return response;
  };
  const SetPremium = async (data) => {
    const response = await api.post('User/NewSubscription', {...data, platform: Platform.OS});
    return response;
  };
  const GetPatentReportFree = async (plateNumber) => {
    const response = await api.get(`Report/GetFree?plate_number=${plateNumber}`);
    return response;
  };
  const GetPatentReportPremium = async (plateNumber) => {
    const response = await api.get(
      `Report/Create?plate_number=${plateNumber}&user_id=${auth().currentUser.uid}`
    );
    return response;
  };
  const GetReport = async (reportId) => {
    const response = await api.get(
      `Report/Get?report_id=${reportId}&user_id=${auth().currentUser.uid}`
    );
    return response;
  };
  const GetPatentReportQuick = async (plateNumber) => {
    const response = await api.get(`Report/GetQuick?plate_number=${plateNumber}`);
    return response;
  };
  return {
    GetOnBoard,
    CreateUser,
    GetUser,
    GetHome,
    CheckSubscription,
    MoveSubscription,
    SetPremium,
    GetPatentReportFree,
    GetPatentReportPremium,
    GetPatentReportQuick,
    GetReport,
  };
};

export default {create};
